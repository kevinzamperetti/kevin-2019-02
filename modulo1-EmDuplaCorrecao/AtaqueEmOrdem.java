import java.util.*;

public class AtaqueEmOrdem implements Estrategia{
        private final ArrayList<Status> STATUS_PERMITIDOS = new ArrayList<>(
            Arrays.asList(Status.RECEM_CRIADO, Status.SOFREU_DANO));        
        
    public ArrayList<Elfo> collections(ArrayList<Elfo> elfos){

        Collections.sort(elfos, new Comparator<Elfo>(){
            public int compare(Elfo elfoAtual, Elfo proximoElfo){
                boolean mesmoTipo = elfoAtual.getClass() == proximoElfo.getClass();
                if(mesmoTipo){
                    return 0;
                }
                return elfoAtual instanceof ElfoVerde && proximoElfo instanceof ElfoNoturno ? -1 : 1;
            }
        });
        return elfos;        
    }

    @Override
    public ArrayList<Elfo> getOrdemDeAtaque(ArrayList<Elfo> atacantes){
        return collections(atacantes);
    }   

}
    
 
    
