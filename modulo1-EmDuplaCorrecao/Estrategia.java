import java.util.*;

public interface Estrategia{
    ArrayList<Elfo> getOrdemDeAtaque(ArrayList<Elfo> atacantes);
}
