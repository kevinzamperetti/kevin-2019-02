import java.util.*;

public class PaginadorInventario{
    private Inventario inventario;
    private int marcador = 0;

    public PaginadorInventario(Inventario inventario){
        this.inventario = inventario;
    }
    
    /*
    public ArrayList<Item> pular(int marcador){
        ArrayList<Item> itens = this.inventario.getItens();
        ArrayList<Item> itensPular = new ArrayList<>();
        for(int i=qtdPular; i<itens.size(); i++){
            itensPular.add(itens.get(i));
        }
        return itensPular;
    }
   
    public ArrayList<Item> limitar(int qtdLimitar, int qtdPular){
        ArrayList<Item> itens = inventario.getItens();
        ArrayList<Item> itensLimitar = new ArrayList<>();
        itens = this.pular(qtdPular);

        for(int i=0; i<qtdLimitar; i++){
            itensLimitar.add(itens.get(i));
        }
        return itensLimitar;
    }
    */
    
    public void pular(int marcador){
        this.marcador = marcador > 0 ? marcador : 0;
    }    
    
    public ArrayList<Item> limitar(int qtdLimitar){

        ArrayList<Item> subConjunto = new ArrayList<>();
        int fim = this.marcador + qtdLimitar;

        for(int i = this.marcador; i < fim && i < this.inventario.getItens().size(); i++){
            subConjunto.add(this.inventario.obter(i));
        }
        return subConjunto;
    }
    
    
}
