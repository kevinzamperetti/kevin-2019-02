import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class AtaqueEmOrdemTest{
    @Test
    public void alistarElfosEVerAtacantes() {

        AtaqueEmOrdem ataqueEmOrdem = new AtaqueEmOrdem();
        
        ElfoNoturno elfoNoturno = new ElfoNoturno("Elfo Noturno 1");
        ElfoNoturno elfoNoturno2 = new ElfoNoturno("Elfo Noturno 2");
        ElfoVerde elfoVerde = new ElfoVerde("Elfo 1");
        ElfoNoturno elfoNoturno3 = new ElfoNoturno("Elfo Noturno 3");
        ElfoVerde elfoVerde2 = new ElfoVerde("Elfo 2");
        ElfoNoturno elfoNoturno4 = new ElfoNoturno("Elfo Noturno 4");
        
        ArrayList<Elfo> elfosEnviados = new ArrayList<>(
            Arrays.asList(elfoNoturno,elfoNoturno2,elfoVerde,elfoNoturno3,elfoVerde2,elfoNoturno4)
        );
        ArrayList<Elfo> elfosEsperados = new ArrayList<>(
            Arrays.asList(elfoVerde, elfoVerde2, elfoNoturno, elfoNoturno2, elfoNoturno3, elfoNoturno4)
        );
        ArrayList<Elfo> elfosResultado = ataqueEmOrdem.getOrdemDeAtaque(elfosEnviados);
        assertEquals(elfosEnviados, elfosResultado);
    }
}
