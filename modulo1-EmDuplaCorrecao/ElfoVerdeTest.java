import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoVerdeTest{
    
    @Test
    public void elfoVerdeNasceComArcoEFlecha(){
        ElfoVerde novoElfoVerde = new ElfoVerde("Elfo Verde");
        assertEquals("Arco, Flecha", novoElfoVerde.inventario.getDescricoesItens());
    }
    
    @Test
    public void elfoVerdeAtiraFlechaGanhaDobroDeXp(){
        ElfoVerde novoElfoVerde = new ElfoVerde("Elfo Verde");
        Dwarf novoDwarf = new Dwarf("Gimli");
        novoElfoVerde.atirarFlecha(novoDwarf);
        assertEquals(2, novoElfoVerde.getExperiencia());
    }    
    
    @Test
    public void elfoVerdeNaoPodePerderArcoNormal(){
        ElfoVerde novoElfoVerde = new ElfoVerde("Elfo Verde");
        Dwarf novoDwarf = new Dwarf("Gimli");
        novoElfoVerde.perderItem(new Item(1, "Arco"));
        assertEquals("Arco, Flecha", novoElfoVerde.inventario.getDescricoesItens());
    }    
    
    @Test
    public void elfoVerdeGanhaArcoDeVidro(){
        ElfoVerde novoElfoVerde = new ElfoVerde("Elfo Verde");
        Dwarf novoDwarf = new Dwarf("Gimli");
        novoElfoVerde.ganharItem(new Item(1, "Arco de Vidro"));
        assertEquals("Arco, Flecha, Arco de Vidro", novoElfoVerde.inventario.getDescricoesItens());
    }     

    @Test
    public void elfoVerdeGanharEPerdeArcoDeVidro(){
        ElfoVerde novoElfoVerde = new ElfoVerde("Elfo Verde");
        Dwarf novoDwarf = new Dwarf("Gimli");
        novoElfoVerde.ganharItem(new Item(1, "Arco de Vidro"));
        novoElfoVerde.perderItem(new Item(1, "Arco de Vidro"));
        assertEquals("Arco, Flecha", novoElfoVerde.inventario.getDescricoesItens());
    }         
    
    @Test
    public void elfoVerdeGanhaArcoDeVidroENaoPodePerdeArcoNormal(){
        ElfoVerde novoElfoVerde = new ElfoVerde("Elfo Verde");
        Dwarf novoDwarf = new Dwarf("Gimli");
        novoElfoVerde.ganharItem(new Item(1, "Arco de Vidro"));
        novoElfoVerde.perderItem(new Item(1, "Arco"));
        assertEquals("Arco, Flecha, Arco de Vidro", novoElfoVerde.inventario.getDescricoesItens());
    }     

    @Test
    public void elfoVerdeGanhaItemComDescricaoInvalida(){
        ElfoVerde novoElfoVerde = new ElfoVerde("Elfo Verde");
        Dwarf novoDwarf = new Dwarf("Gimli");
        Inventario inventario = novoElfoVerde.getInventario();
        novoElfoVerde.ganharItem(new Item(1, "Arco de Madeira"));
        assertEquals("Arco, Flecha", novoElfoVerde.inventario.getDescricoesItens());
        assertNull(inventario.buscarItem("Arco de Madeira"));
    }
}
