public class ExercitoQueAtaca extends ExercitoElfo{
    private Estrategia estrategia;
    
    public ExercitoQueAtaca(Estrategia estrategia){
        this.estrategia = estrategia;
    }
    
    public void trocarEstrategia(Estrategia estrategia){
        this.estrategia = estrategia;
    }
    
    
}
