import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class ElfoNoturnoTest{
    private final double DELTA = 1e-9;
    
    @Test
    public void elfoNoturnoAtiraFlechaGanha3XpPerde15DeVidaDwarfPerde10DeVida(){
        ElfoNoturno novoElfoNoturno = new ElfoNoturno("Elfo Noturno");
        Dwarf novoDwarf = new Dwarf("Gimli");
        novoElfoNoturno.atirarFlecha(novoDwarf);
        assertEquals(3, novoElfoNoturno.getExperiencia(), DELTA);
        assertEquals(85, novoElfoNoturno.getQtdVida(), DELTA);
        assertEquals(100, novoDwarf.getQtdVida(), DELTA);
    }
    
    @Test
    public void elfoNoturnoAtiraFlechaEPerde15DeVidaEStatusDosDoisFicaSofreuDano(){
        ElfoNoturno novoElfoNoturno = new ElfoNoturno("Elfo Noturno");
        Dwarf novoDwarf = new Dwarf("Gimli");
        novoElfoNoturno.atirarFlecha(novoDwarf);
        assertEquals(85.0,novoElfoNoturno.getQtdVida(), DELTA);
        assertEquals(Status.SOFREU_DANO, novoElfoNoturno.getStatus());
        assertEquals(Status.SOFREU_DANO, novoDwarf.getStatus());
    }
    
    @Test
    public void elfoNoturnoPerdeTodaVidaEStatusFicaMortoDwarfSofreuDano(){
        ElfoNoturno novoElfoNoturno = new ElfoNoturno("Elfo Noturno");
        Dwarf novoDwarf = new Dwarf("Gimli");
        novoElfoNoturno.getFlecha().setQuantidade(100);
        for(int i = 0; i < 8; i++ ){
            novoElfoNoturno.atirarFlecha(novoDwarf);
        }
        assertEquals(Status.MORTO,novoElfoNoturno.getStatus());
        assertEquals(Status.SOFREU_DANO,novoDwarf.getStatus());
        assertEquals(0.0,novoElfoNoturno.getQtdVida(), DELTA);
    }
    
//    elfoNoturnoGanha3XpAoAtirarFlecha
//    elfoNoturnoAtiraFlechaEPerde15

}
