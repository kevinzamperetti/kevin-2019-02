import java.util.*;

public interface Estrategia{
    ArrayList<Elfo> getOrdemDeAtaque(ArrayList<Elfo> atacantes);
    ArrayList<Elfo> getAtaqueIntercalado(ArrayList<Elfo> ordemAtaque);
    ArrayList<Elfo> getAtaqueElfosVivosComFlecha(ArrayList<Elfo> atacantes);
}
