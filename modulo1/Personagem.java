public abstract class Personagem{  //para não ser instanciada
    protected String nome;
    protected Status status;
    protected Inventario inventario;
    protected double qtdVida;
    protected int experiencia;
    protected int qtdExperienciaPorAtaque;
    protected double qtdDano; 

    {
        status = Status.RECEM_CRIADO;
        inventario = new Inventario();
        experiencia = 0;
    }
    
    protected Personagem(){
       this.nome = nome;
    }    
    
    protected Personagem(String nome){
        this.nome = nome;
    }
    
    protected String getNome(){
        return this.nome;
    }
    
    protected void setNome(String nome){
        this.nome = nome;
    }

    protected Status getStatus(){
        return this.status;
    }  
    
    protected void setStatus(Status status){
        this.status = status;
    }
    
    protected Inventario getInventario(){  
        return this.inventario;
    }    
    
    protected double getQtdVida(){
        return qtdVida;
    }
    
    protected int getExperiencia(){
        return this.experiencia;
    }    

    protected void aumentarXp(){
        this.experiencia += this.qtdExperienciaPorAtaque;
    } 
    
    protected void ganharItem(Item itemNovo){
        this.inventario.adicionar(itemNovo);
    }

    protected void perderItem(Item itemExcluir){
        this.inventario.remover(itemExcluir);
    }    
    
    protected boolean podeSofrerDano(){
        return this.qtdVida > 0;
    }
    
    protected double calcularDano(){
        return this.qtdDano;
    }
    
    protected void sofrerDano(){
        if(this.podeSofrerDano()){
            this.qtdVida -= this.qtdVida >= this.calcularDano() ? this.calcularDano() : this.qtdVida;
        }
        this.status = this.qtdVida == 0 ? Status.MORTO : Status.SOFREU_DANO;
    }     
    
    protected abstract String imprimirResultado();
    
}
