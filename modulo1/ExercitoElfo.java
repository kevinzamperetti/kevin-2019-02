    import java.util.*;
    
    public class ExercitoElfo implements Estrategia{
        private final ArrayList<Class> TIPOS_PERMITIDOS = new ArrayList<>(
            Arrays.asList(ElfoVerde.class, ElfoNoturno.class));
            
        private final ArrayList<Status> STATUS_PERMITIDOS = new ArrayList<>(
            Arrays.asList(Status.RECEM_CRIADO, Status.SOFREU_DANO));        
    
        private HashMap<Status, ArrayList<Elfo>> porStatus = new HashMap<>();
    
        private ArrayList<Elfo> elfos = new ArrayList<>(); //lista de elfos de todos status
        
        public void alistarElfo(Elfo elfo){
            boolean tipoValido = TIPOS_PERMITIDOS.contains(elfo.getClass());
            if(tipoValido){
                elfos.add(elfo);
                ArrayList<Elfo> elfoDoStatus = porStatus.get(elfo.getStatus()); //pega um elfo de um status especifico
                if(elfoDoStatus == null){
                    elfoDoStatus = new ArrayList<>();
                    porStatus.put(elfo.getStatus(), elfoDoStatus);  //
                }
                elfoDoStatus.add(elfo);
            }
        }
    
        public ArrayList<Elfo> getElfos(){
            return this.elfos;
        }
        
        public ArrayList<Elfo> buscar(Status status){
            return this.porStatus.get(status);
        }
        
	public HashMap<Class,ArrayList<Elfo>> separarElfosPorClasse(ArrayList<Elfo> exercitoElfos){
            ArrayList<Elfo> elfosNoturnos = new ArrayList<>();
            ArrayList<Elfo> elfosVerdes = new ArrayList<>();
            HashMap<Class,ArrayList<Elfo>> separarClasses = new HashMap<>();
            
            for(Elfo elfo : exercitoElfos){
                boolean elfoVivo = STATUS_PERMITIDOS.contains(elfo.getStatus());
                Class classeDoElfo = elfo.getClass();
                if(elfoVivo){
                    if(ElfoVerde.class == classeDoElfo){
                        elfosVerdes.add(elfo);
                        separarClasses.put(classeDoElfo,elfosVerdes);
                    }else if(ElfoNoturno.class == elfo.getClass()){
                        elfosNoturnos.add(elfo);
                        separarClasses.put(classeDoElfo,elfosNoturnos);
                    }
                }
            }
            return separarClasses;
        }
    
        @Override
        public ArrayList<Elfo> getOrdemDeAtaque(ArrayList<Elfo> atacantes){
            ArrayList<Elfo> elfosAtacantes = new ArrayList<>();
            HashMap<Class,ArrayList<Elfo>> elfosSeparados = separarElfosPorClasse(atacantes);
            
            for(Elfo elfo : elfosSeparados.get(ElfoVerde.class)){
                elfosAtacantes.add(elfo);
            }
            for(Elfo elfo : elfosSeparados.get(ElfoNoturno.class)){
                elfosAtacantes.add(elfo);
            }
            
            return elfosAtacantes;
        }
    
        @Override
        public ArrayList<Elfo> getAtaqueIntercalado(ArrayList<Elfo> ordemAtaque){
            ArrayList<Elfo> ordemAtaqueIntercalado = new ArrayList<>();
//            HashMap<Class,ArrayList<Elfo>> elfosSeparados = separarElfosPorClasse(ordemAtaque);		
            Class ultimoAtaque = null;
            for(Elfo elfo : ordemAtaque){
                if(elfo.getClass().equals(ElfoVerde.class) &&
                  (ultimoAtaque.equals(ElfoNoturno.class) || ultimoAtaque == null)){
                    ultimoAtaque = elfo.getClass();
                    ordemAtaqueIntercalado.add(elfo);
                }else{
                    ultimoAtaque = ElfoNoturno.class;
                    ordemAtaqueIntercalado.add(elfo);
                }
            }
            return ordemAtaqueIntercalado;
        }
    
        @Override
        public ArrayList<Elfo> getAtaqueElfosVivosComFlecha(ArrayList<Elfo> atacantes){
            ArrayList<Elfo> elfosAtacantes = new ArrayList<>();
            //lógica da estratégia
            return elfosAtacantes;
        }
}
