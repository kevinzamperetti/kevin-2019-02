import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoDaLuzTest{
    private final double DELTA = 1e-9;    
    
    @Test
    public void elfoDaLuzPrimeiroAtaquePerde21DeVidaSegundoAtaqueGanha10DeVida(){
        ElfoDaLuz feanor = new ElfoDaLuz("Fëanor");
	feanor.atacarComEspada(new Dwarf("Farlum"));    // perde 21 unidades, foi o ataque 1
	assertEquals(79.0, feanor.getQtdVida(), DELTA);

	feanor.atacarComEspada(new Dwarf("Gul"));       // ganha 10 unidades, foi o ataque 2
	assertEquals(89.0, feanor.getQtdVida(), DELTA);
    }
    
    @Test
    public void elfoDaLuzNasceComArcoFlechaEspadaDeGalvorn(){
        ElfoDaLuz feanor = new ElfoDaLuz("Fëanor");
        assertEquals("Arco, Flecha, Espada de Galvorn", feanor.getInventario().getDescricoesItens());
    }
    
    @Test
    public void elfoDaLuzFazDoisAtaquesGanhaDoisDeXp(){
        ElfoDaLuz feanor = new ElfoDaLuz("Fëanor");
	feanor.atacarComEspada(new Dwarf("Farlum")); 
	feanor.atacarComEspada(new Dwarf("Gul")); 
	assertEquals(2, feanor.getExperiencia(), DELTA); 
    }    
    
    @Test
    public void elfoDaLuzNaoPodePerderEspadaDeGalvorn(){
        ElfoDaLuz feanor = new ElfoDaLuz("Fëanor");
        feanor.perderItem(new Item(1, "Espada de Galvorn"));
        assertEquals("Arco, Flecha, Espada de Galvorn", feanor.getInventario().getDescricoesItens());
    }
    
}
