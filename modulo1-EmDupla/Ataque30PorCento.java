import java.util.*;

public class Ataque30PorCento implements Estrategia{
    private final ArrayList<Status> STATUS_PERMITIDOS = new ArrayList<>(
    Arrays.asList(Status.RECEM_CRIADO, Status.SOFREU_DANO));                  
    
    public HashMap<Class,ArrayList<Elfo>> separarElfosPorClasse(ArrayList<Elfo> exercitoElfos){
        ArrayList<Elfo> elfosNoturnos = new ArrayList<>();
        ArrayList<Elfo> elfosVerdes = new ArrayList<>();
        HashMap<Class,ArrayList<Elfo>> separarClasses = new HashMap<>();
        for(Elfo elfo : exercitoElfos){
            boolean elfoVivo = STATUS_PERMITIDOS.contains(elfo.getStatus());
            Class classeDoElfo = elfo.getClass();
            if(elfoVivo){
                if(ElfoVerde.class == classeDoElfo){
                        elfosVerdes.add(elfo);
                        separarClasses.put(classeDoElfo,elfosVerdes);
                    }else if(ElfoNoturno.class == elfo.getClass()){
                        elfosNoturnos.add(elfo);
                        separarClasses.put(classeDoElfo,elfosNoturnos);
                    }
                }
        }
        return separarClasses;
    }
    
    @Override
    public ArrayList<Elfo> getOrdemDeAtaque(ArrayList<Elfo> ordemAtaque){
        ArrayList<Elfo> listaDeAtaque30PorCento = new ArrayList<>();
        HashMap<Class,ArrayList<Elfo>> elfosSeparados = separarElfosPorClasse(ordemAtaque);      
        int totalElfoNoExercito = ordemAtaque.size();
        int qtdElfosVerdes = (int)(totalElfoNoExercito * 0.7);
        int qtdElfosNoturnos = (int)(totalElfoNoExercito * 0.3);
        for(int i = 0; i < qtdElfosVerdes; i++){
              listaDeAtaque30PorCento.add(elfosSeparados.get(ElfoVerde.class).get(i));
        }
        for(int i = 0; i < qtdElfosNoturnos; i++){
              listaDeAtaque30PorCento.add(elfosSeparados.get(ElfoNoturno.class).get(i));
        }
        return listaDeAtaque30PorCento;
     }
}
