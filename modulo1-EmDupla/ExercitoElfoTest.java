import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class ExercitoElfoTest{

    @Test
    public void totalVerdesETotalNoturnos(){
        ElfoVerde elfoVerde = new ElfoVerde("Elfo 1");
        ElfoNoturno elfoNoturno = new ElfoNoturno("Elfo Noturno 1");
        ElfoVerde elfoVerde2 = new ElfoVerde("Elfo 2");
        ElfoNoturno elfoNoturno2 = new ElfoNoturno("Elfo Noturno 2");
        ElfoVerde elfoVerde3 = new ElfoVerde("Elfo 3");
        ElfoNoturno elfoNoturno3 = new ElfoNoturno("Elfo Noturno 3");
        ElfoVerde elfoVerde4 = new ElfoVerde("Elfo 4");
        ElfoNoturno elfoNoturno4 = new ElfoNoturno("Elfo Noturno 4");
    
        ExercitoElfo exercito = new ExercitoElfo();
        exercito.alistarElfo(elfoVerde);
        exercito.alistarElfo(elfoNoturno);
        exercito.alistarElfo(elfoVerde2);
        exercito.alistarElfo(elfoNoturno2);
        exercito.alistarElfo(elfoVerde3);
        exercito.alistarElfo(elfoNoturno3);
        exercito.alistarElfo(elfoVerde4);

        assertEquals(4,exercito.separarElfosPorClasse(exercito.getElfos()).get(ElfoVerde.class).size());        
    }
    
    /*@Test
    public void alistarElfosEVerAtacantes() {
        ElfoVerde elfoVerde = new ElfoVerde("Elfo 1");
        ElfoNoturno elfoNoturno = new ElfoNoturno("Elfo Noturno 1");
        ElfoVerde elfoVerde2 = new ElfoVerde("Elfo 2");
        ElfoNoturno elfoNoturno2 = new ElfoNoturno("Elfo Noturno 2");
        
        ExercitoElfo exercito = new ExercitoElfo();
        exercito.alistarElfo(elfoVerde);
        exercito.alistarElfo(elfoNoturno);
        exercito.alistarElfo(elfoVerde2);
        exercito.alistarElfo(elfoNoturno2);

        elfoNoturno2.setStatus(Status.MORTO);
       
        assertEquals(Arrays.asList(elfoVerde, elfoVerde2, elfoNoturno), 
        exercito.getOrdemDeAtaque(exercito.getElfos()));
    }
    

        
    
  
    
    /*@Test
    public void alistarElfosEVerOrdemDeAtaqueIntercalada(){
        ElfoVerde elfoVerde = new ElfoVerde("Elfo 1");
        ElfoNoturno elfoNoturno = new ElfoNoturno("Elfo Noturno 1");
        ElfoVerde elfoVerde2 = new ElfoVerde("Elfo 2");
        ElfoNoturno elfoNoturno2 = new ElfoNoturno("Elfo Noturno 2");
        ElfoVerde elfoVerde3 = new ElfoVerde("Elfo 3");
        ElfoNoturno elfoNoturno3 = new ElfoNoturno("Elfo Noturno 3");
        ElfoVerde elfoVerde4 = new ElfoVerde("Elfo 4");
        ElfoNoturno elfoNoturno4 = new ElfoNoturno("Elfo Noturno 4");
        
        ExercitoElfo exercito = new ExercitoElfo();
        exercito.alistarElfo(elfoVerde);
        exercito.alistarElfo(elfoNoturno);
        exercito.alistarElfo(elfoVerde2);
        exercito.alistarElfo(elfoNoturno2);
        exercito.alistarElfo(elfoVerde3);
        exercito.alistarElfo(elfoNoturno3);
        exercito.alistarElfo(elfoVerde4);
        exercito.alistarElfo(elfoNoturno4);

        ArrayList<Elfo> teste = new ArrayList<>();
        teste = exercito.getOrdemDeAtaque(exercito.getElfos());
        assertEquals(Arrays.asList(elfoVerde, elfoVerde2, elfoNoturno, elfoNoturno2), 
        exercito.getAtaqueIntercalado(teste));
    }*/
}
