import java.util.*;

public class AtaqueEmOrdem implements Estrategia{
        private final ArrayList<Status> STATUS_PERMITIDOS = new ArrayList<>(
            Arrays.asList(Status.RECEM_CRIADO, Status.SOFREU_DANO));        
        
        public HashMap<Class,ArrayList<Elfo>> separarElfosPorClasse(ArrayList<Elfo> exercitoElfos){
            ArrayList<Elfo> elfosNoturnos = new ArrayList<>();
            ArrayList<Elfo> elfosVerdes = new ArrayList<>();
            HashMap<Class,ArrayList<Elfo>> separarClasses = new HashMap<>();
            
            for(Elfo elfo : exercitoElfos){
                boolean elfoVivo = STATUS_PERMITIDOS.contains(elfo.getStatus());
                Class classeDoElfo = elfo.getClass();
                if(elfoVivo){
                    if(ElfoVerde.class == classeDoElfo){
                        elfosVerdes.add(elfo);
                        separarClasses.put(classeDoElfo,elfosVerdes);
                    }else if(ElfoNoturno.class == elfo.getClass()){
                        elfosNoturnos.add(elfo);
                        separarClasses.put(classeDoElfo,elfosNoturnos);
                    }
                }
            }
            return separarClasses;
        }

    @Override
    public ArrayList<Elfo> getOrdemDeAtaque(ArrayList<Elfo> atacantes){
        ArrayList<Elfo> elfosAtacantes = new ArrayList<>();
        HashMap<Class,ArrayList<Elfo>> elfosSeparados = separarElfosPorClasse(atacantes);
        int totalElfosVerdes = elfosSeparados.get(ElfoVerde.class).size();
        int totalElfosNoturnos = elfosSeparados.get(ElfoNoturno.class).size();           
        int qtdElfosVerdes = totalElfosVerdes / 2;
        int qtdElfosNoturnos = totalElfosNoturnos / 2;
            
        for(Elfo elfo : elfosSeparados.get(ElfoVerde.class)){
            elfosAtacantes.add(elfo);
        }
        for(Elfo elfo : elfosSeparados.get(ElfoNoturno.class)){
            elfosAtacantes.add(elfo);
        }
            
        return elfosAtacantes;
    }
}
