import java.util.*;

public interface Estrategia{
    HashMap<Class,ArrayList<Elfo>> separarElfosPorClasse(ArrayList<Elfo> exercitoElfos);
    ArrayList<Elfo> getOrdemDeAtaque(ArrayList<Elfo> atacantes);
}
