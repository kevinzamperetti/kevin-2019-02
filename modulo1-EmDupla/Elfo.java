public class Elfo extends Personagem /*implements AtacarDwarf*/{
    protected int indiceFlecha;
    private static int qtdElfos = 0;
    
    {
        this.indiceFlecha = 1;
    }    
    
    protected Elfo(String nome){
        super(nome);
        this.inventario.adicionar(new Item(1, "Arco"));
        this.inventario.adicionar(new Item(2, "Flecha"));
        this.qtdVida = 100.0;
        this.qtdDano = 0.0;
        this.qtdExperienciaPorAtaque = 1;
        Elfo.qtdElfos++;
    }
    
    protected void finalize() throws Throwable{
        Elfo.qtdElfos--;
    }
    
    protected static int getQtdElfos(){
        return Elfo.qtdElfos;
    }
    
    protected Item getFlecha(){
        //return this.inventario.getItemPorDescricao("Flecha");
        return this.inventario.obter(indiceFlecha);
    }
    
    protected int getQtdFlecha(){
         return this.getFlecha().getQuantidade();
    }

    protected boolean podeAtirarFecha(){
        return this.getQtdFlecha() > 0;
    }

    protected void atirarFlecha(Dwarf dwarf){
        int qtdAtual = this.getQtdFlecha();
        if(this.podeAtirarFecha() && this.podeSofrerDano()){
            this.getFlecha().setQuantidade(qtdAtual-1);
            this.aumentarXp();
            double dwarfVida = dwarf.getQtdVida();
            dwarf.sofrerDano();
            this.sofrerDano();
        }
    }
    
    // public int atacar(){
        // atirarFlecha;
        // return 1
    // }
    
    protected TipoElfo getTipoElfo() {
        return TipoElfo.NAO_DEFINIDO;
    }     
    
    @Override
    protected String imprimirResultado(){
        return "Elfo";
    }    
}
