import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class InventarioTest{

    // @Test  
    // public void criarInventarioComQuantidadeInformada(){
        // Inventario mochila= new Inventario();
        // assertEquals(34, mochila.getTamanhoMochila());
    // }

    // @Test 
    // public void adicionarDoisItensComEspacoParaUmNaoAdicionaSegundo(){
        // Inventario mochila= new Inventario();
        // Item espada = new Item(2, "Espada");
        // Item escudo = new Item(2, "Escudo");
        // mochila.adicionar(espada);
        // mochila.adicionar(escudo);     
        
        // assertEquals(espada, mochila.obter(0));
        // assertEquals(1, mochila.getTamanhoMochila());
    // }    
    
    @Test  
    public void criarInventarioSemQuantidadeInformada(){
        Inventario mochila= new Inventario();
        assertEquals(0, mochila.getTamanhoMochila());
    }
    
    @Test  
    public void adicionarItem(){
        Inventario mochila = new Inventario();
        Item espada = new Item(2, "Espada");
        Item escudo = new Item(2, "Escudo");
        mochila.adicionar(espada);
        mochila.adicionar(escudo);     
        assertEquals(espada, mochila.obter(0));
        assertEquals(escudo, mochila.obter(1));
    }
   
    @Test
    public void removerItem(){
        Inventario mochila= new Inventario();
        Item espada = new Item(2, "Espada");
        Item escudo = new Item(2, "Escudo");
        Item capacete = new Item(2, "Capacete");
        mochila.adicionar(espada);
        mochila.adicionar(escudo);
        mochila.adicionar(capacete);
        mochila.remover(escudo);
        assertEquals(espada,mochila.obter(0));
        assertEquals(capacete,mochila.obter(1));
    }
    
    @Test
    public void removerItemAntesDeAdicionarProximo(){
        Inventario mochila= new Inventario();
        Item espada = new Item(2, "Espada");
        Item escudo = new Item(2, "Escudo");
        Item capacete = new Item(2, "Capacete");
        mochila.adicionar(espada);
        mochila.adicionar(escudo);
        mochila.remover(escudo);
        mochila.adicionar(capacete);
        assertEquals(espada,mochila.obter(0));
        assertEquals(capacete,mochila.obter(1));
    }

    @Test
    public void imprimeDescricaoItens(){
        Inventario mochila= new Inventario();
        Item espada = new Item(2, "Espada");
        Item escudo = new Item(2, "Escudo");
        Item capacete = new Item(2, "Capacete");
        mochila.adicionar(espada);
        mochila.adicionar(escudo);
        mochila.adicionar(capacete);
        assertEquals("Espada, Escudo, Capacete", mochila.getDescricoesItens());
    }
    
    @Test
    public void imprimeDescricaoItensNenhumItem(){
        Inventario mochila= new Inventario();
        assertEquals("", mochila.getDescricoesItens());
    }    
    
    @Test
    public void retornarItemComMaiorQuantidade(){
        Inventario mochila= new Inventario();
        Item espada = new Item(2, "Espada");
        Item escudo = new Item(20, "Escudo");
        Item capacete = new Item(12, "Capacete");
        mochila.adicionar(espada);
        mochila.adicionar(escudo);
        mochila.adicionar(capacete);
        assertEquals(escudo, mochila.retornarItemComMaiorQuantidade());
    }
    
    @Test
    public void retornarMaiorQuantidadeInventarioSemItens(){
        Inventario mochila= new Inventario();
        assertNull(mochila.retornarItemComMaiorQuantidade());
    }    
    
    @Test
    public void retornarItensComMesmaQuantidade(){
        Inventario mochila= new Inventario();
        Item espada = new Item(2, "Espada");
        Item escudo = new Item(20, "Escudo");
        Item capacete = new Item(20, "Capacete");
        mochila.adicionar(espada);
        mochila.adicionar(escudo);
        mochila.adicionar(capacete);
        assertEquals(escudo, mochila.retornarItemComMaiorQuantidade());
    }   
    
    @Test
    public void buscarDescricaoDoItemExistenteEOutroNaoExistente(){
        Inventario mochila= new Inventario();
        Item espada = new Item(2, "Espada");
        Item flecha = new Item(20, "Flecha");
        Item capacete = new Item(12, "Capacete");
        mochila.adicionar(espada);
        mochila.adicionar(flecha);
        mochila.adicionar(capacete);
        assertEquals(flecha, mochila.buscarItem("Flecha"));        
        assertNull(mochila.buscarItem("Arco"));        
    }

    @Test
    public void inverterInventarioVazio(){
        Inventario mochila= new Inventario();
        ArrayList<Item> inverso = new ArrayList<>();
        assertTrue(mochila.inverterItensInventario().isEmpty());        
    }     

    @Test
    public void inverterItensInventarioComApenasDoisItens(){
        Inventario mochila= new Inventario();
        Item espada = new Item(2, "Espada");
        Item flecha = new Item(20, "Flecha");
        mochila.adicionar(espada);
        mochila.adicionar(flecha);
        assertEquals(flecha, mochila.inverterItensInventario().get(0));        
        assertEquals(espada, mochila.inverterItensInventario().get(1));        
        //assertEquals(2, mochila.getItens().size());        
    } 
    
    @Test
    public void inverterItensInventario(){
        Inventario mochila= new Inventario();
        ArrayList<Item> inverso = new ArrayList<>();
        Item espada = new Item(2, "Espada");
        Item flecha = new Item(20, "Flecha");
        Item capacete = new Item(12, "Capacete");
        mochila.adicionar(espada);
        mochila.adicionar(flecha);
        mochila.adicionar(capacete);
        inverso.add(capacete);
        inverso.add(flecha);
        inverso.add(espada);
        assertEquals(inverso, mochila.inverterItensInventario());        
    }    

/*    
    @Test
    public void ordenarItensInventario(){
        Inventario mochila= new Inventario();
        ArrayList<Item> ordenado = new ArrayList<>();
        Item espada = new Item(2, "Espada");
        Item escudo = new Item(20, "Escudo");
        Item capacete = new Item(1, "Capacete");
        mochila.adicionar(espada);
        mochila.adicionar(escudo);
        mochila.adicionar(capacete);
        ordenado.add(capacete);
        ordenado.add(espada);
        ordenado.add(escudo);
        mochila.ordenarItensInventario();
        assertEquals(ordenado,mochila.getItens());
    }
    
    @Test
    public void ordenarItensComTipoOrdenacaoAsc(){
        Inventario mochila = new Inventario();
        Inventario mochilaOrdenada = new Inventario();
        ArrayList<Item> ordenadoAsc = new ArrayList<>();

        Item espada = new Item(2, "Espada");
        Item escudo = new Item(20, "Escudo");
        Item capacete = new Item(1, "Capacete");
        mochila.adicionar(espada);
        mochila.adicionar(escudo);
        mochila.adicionar(capacete);

        mochilaOrdenada.adicionar(capacete);
        mochilaOrdenada.adicionar(espada);
        mochilaOrdenada.adicionar(escudo);
        
        mochila.ordenarItensInventario(TipoOrdenacao.ASC);
        assertEquals(mochila, mochilaOrdenada);
    }    
    
    @Test
    public void ordenarItensComTipoOrdenacaoDesc(){
        Inventario mochila= new Inventario();
        ArrayList<Item> ordenadoDesc = new ArrayList<>();
        Item espada = new Item(50, "Espada");
        Item escudo = new Item(20, "Escudo");
        Item capacete = new Item(100, "Capacete");
        mochila.adicionar(espada);
        mochila.adicionar(escudo);
        mochila.adicionar(capacete);
        ordenadoDesc.add(capacete);
        ordenadoDesc.add(espada);
        ordenadoDesc.add(escudo);
        mochila.ordenarItensInventario(TipoOrdenacao.DESC);
        assertEquals(ordenadoDesc,mochila.getItens());
        
    }    
*/
    
}
