import java.util.*;

public class AtaqueIntercalado implements Estrategia{
        public Sorteador sorteador;
    
        private final ArrayList<Status> STATUS_PERMITIDOS = new ArrayList<>(
            Arrays.asList(Status.RECEM_CRIADO, Status.SOFREU_DANO));        
        
        public AtaqueIntercalado(){
            sorteador = new DadoD6();
        }            
        
        public HashMap<Class,ArrayList<Elfo>> separarElfosPorClasse(ArrayList<Elfo> exercitoElfos){
            ArrayList<Elfo> elfosNoturnos = new ArrayList<>();
            ArrayList<Elfo> elfosVerdes = new ArrayList<>();
            HashMap<Class,ArrayList<Elfo>> separarClasses = new HashMap<>();
            
            for(Elfo elfo : exercitoElfos){
                boolean elfoVivo = STATUS_PERMITIDOS.contains(elfo.getStatus());
                Class classeDoElfo = elfo.getClass();
                if(elfoVivo){
                    if(ElfoVerde.class == classeDoElfo){
                        elfosVerdes.add(elfo);
                        separarClasses.put(classeDoElfo,elfosVerdes);
                    }else if(ElfoNoturno.class == elfo.getClass()){
                        elfosNoturnos.add(elfo);
                        separarClasses.put(classeDoElfo,elfosNoturnos);
                    }
                }
            }
            return separarClasses;
        }

        @Override
        public ArrayList<Elfo> getOrdemDeAtaque(ArrayList<Elfo> ordemAtaque){
            ArrayList<Elfo> listaDeAtaqueIntercalado = new ArrayList<>();
            HashMap<Class,ArrayList<Elfo>> elfosSeparados = separarElfosPorClasse(ordemAtaque);      
            
            int qtdElfosVerdes = elfosSeparados.get(ElfoVerde.class).size();//2
            int qtdElfosNoturnos = elfosSeparados.get(ElfoNoturno.class).size();//1
            int totalElfoNoExercito = qtdElfosVerdes <= qtdElfosNoturnos ? qtdElfosVerdes: qtdElfosNoturnos;
            int ultimoAtaque = 0;
            
            int numeroDado = sorteador.sortear();
            for(int i = 0; i < totalElfoNoExercito; i++){
                if(numeroDado <= 3){
                    listaDeAtaqueIntercalado.add(elfosSeparados.get(ElfoVerde.class).get(i));
                    listaDeAtaqueIntercalado.add(elfosSeparados.get(ElfoNoturno.class).get(i));
                }else{
                    listaDeAtaqueIntercalado.add(elfosSeparados.get(ElfoNoturno.class).get(i));
                    listaDeAtaqueIntercalado.add(elfosSeparados.get(ElfoVerde.class).get(i));
                }
            }
            return listaDeAtaqueIntercalado;
        }
}
