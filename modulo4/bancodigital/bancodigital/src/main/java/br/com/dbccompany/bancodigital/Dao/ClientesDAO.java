package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.ClientesDTO;
import br.com.dbccompany.bancodigital.Entity.Clientes;
import br.com.dbccompany.bancodigital.Entity.Enderecos;

public class ClientesDAO extends AbstractDAO<Clientes>{

	private static final EnderecosDAO ENDERECOS_DAO = new EnderecosDAO(); 
	
	public Clientes parseFrom( ClientesDTO dto ) {
		Clientes clientes = null;
		if ( dto.getIdCliente() != null ) {
			clientes = buscar( dto.getIdCliente() );
		} else {
			clientes = new Clientes();
		}
		
		Enderecos endereco = ENDERECOS_DAO.parseFrom( dto.getEnderecos() );
		
		clientes.setCpf( dto.getCpf() );
		clientes.setNome( dto.getNome() );
		clientes.setRg( dto.getRg() );
		clientes.setConjuge( dto.getConjuge() );
		clientes.setDataNascimento( dto.getDataNascimento() );
		clientes.setEstadoCivil( dto.getEstadoCivil() );
		clientes.setEndereco( endereco );
		return clientes;
	}	
	
	@Override
	protected Class<Clientes> getEntityClass() {
		return Clientes.class;
	}
}
