package br.com.dbccompany.bancodigital.Dto;

public class AgenciasDTO {

	private Integer idAgencia;
	private Integer codigo;
	private String nome;
	private BancosDTO bancos;
	private EnderecosDTO enderecos;
	
	public Integer getIdAgencia() {
		return idAgencia;
	}
	
	public void setIdAgencia(Integer idAgencia) {
		this.idAgencia = idAgencia;
	}
	
	public Integer getCodigo() {
		return codigo;
	}
	
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public BancosDTO getBancos() {
		return bancos;
	}
	
	public void setBancos(BancosDTO bancos) {
		this.bancos = bancos;
	}
	
	public EnderecosDTO getEnderecos() {
		return enderecos;
	}
	
	public void setEnderecos(EnderecosDTO enderecos) {
		this.enderecos = enderecos;
	}
}
