package br.com.dbccompany.bancodigital.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.Dao.ClientesDAO;
import br.com.dbccompany.bancodigital.Dto.ClientesDTO;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;
import br.com.dbccompany.bancodigital.Entity.Clientes;

public class ClientesService {

	private static final ClientesDAO CLIENTES_DAO = new ClientesDAO(); 
	private static final Logger LOG = Logger.getLogger( ClientesService.class.getName() );
	
	public void salvarClientes( ClientesDTO clientesDTO ) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction  = HibernateUtil.getSession().getTransaction();
		
		Clientes clientes = CLIENTES_DAO.parseFrom( clientesDTO );
		
		try {
			Clientes clientesRes = CLIENTES_DAO.buscar( "nome", clientes.getNome() );
			if ( clientesRes == null ) {
				CLIENTES_DAO.criar( clientes );				
			} else {
				clientes.setId( clientesRes.getId() );
				CLIENTES_DAO.atualizar( clientes );
			}

			if ( started ) {
				transaction.commit();
			}
			
			clientesDTO.setIdCliente( clientes.getId() );
			
		} catch (Exception e) {
			transaction.rollback();
			LOG.log( Level.SEVERE, e.getMessage(), e );
		}
	
	}
}
