package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.AgenciasDTO;
import br.com.dbccompany.bancodigital.Entity.Agencias;
import br.com.dbccompany.bancodigital.Entity.Bancos;
import br.com.dbccompany.bancodigital.Entity.Enderecos;

public class AgenciasDAO extends AbstractDAO<Agencias>{
	
	private static final BancosDAO BANCOS_DAO = new BancosDAO(); 
	private static final EnderecosDAO ENDERECOS_DAO = new EnderecosDAO();  

	public Agencias parseFrom( AgenciasDTO dto ) {
		Agencias agencias = null;
		if ( dto.getIdAgencia() != null ) {
			agencias = buscar( dto.getIdAgencia() );
		} else {
			agencias = new Agencias();
		}
		
		//Enderecos endereco = ENDERECOS_DAO.parseFrom( dto.getEnderecos() );
		Bancos banco = BANCOS_DAO.parseFrom( dto.getBancos() );
		
		agencias.setCodigo( dto.getCodigo() );
		agencias.setNome( dto.getNome() );
		agencias.setBanco( banco );
		//agencias.setEndereco( endereco );
		return agencias;
	}
	
	@Override
	protected Class<Agencias> getEntityClass() {
		return Agencias.class;
	}
}
