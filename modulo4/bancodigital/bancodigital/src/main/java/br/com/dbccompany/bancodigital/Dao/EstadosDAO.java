package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.EstadosDTO;
import br.com.dbccompany.bancodigital.Entity.Estados;
import br.com.dbccompany.bancodigital.Entity.Paises;

public class EstadosDAO extends AbstractDAO<Estados>{
	
	private static final PaisesDAO PAISES_DAO = new PaisesDAO(); 

	public Estados parseFrom( EstadosDTO dto ) {
		Estados estados = null;
		if ( dto.getIdEstado() != null ) {
			estados = buscar( dto.getIdEstado() );
		} else {
			estados = new Estados();
		}
		
		Paises pais = PAISES_DAO.parseFrom( dto.getPaises() );
		
		estados.setNome( dto.getNome() );
		estados.setPais(pais);
		return estados;
	}	
	
	@Override
	protected Class<Estados> getEntityClass() {
		return Estados.class;
	}
}
