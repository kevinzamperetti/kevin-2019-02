package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.EnderecosDTO;
import br.com.dbccompany.bancodigital.Entity.Bairros;
import br.com.dbccompany.bancodigital.Entity.Enderecos;

public class EnderecosDAO extends AbstractDAO<Enderecos>{

	private static final BairrosDAO BAIRROS_DAO = new BairrosDAO(); 
	
	public Enderecos parseFrom( EnderecosDTO dto ) {
		Enderecos enderecos = null;
		if ( dto.getIdEndereco() != null ) {
			enderecos = buscar( dto.getIdEndereco() );
		} else {
			enderecos = new Enderecos();
		}
		
		Bairros bairro = BAIRROS_DAO.parseFrom( dto.getBairros() );
		
		enderecos.setLogradouro( dto.getLogradouro() );
		enderecos.setNumero( dto.getNumero() );
		enderecos.setComplemento( dto.getComplemento() );
		enderecos.setBairro( bairro );
		return enderecos;
	}
	
	@Override
	protected Class<Enderecos> getEntityClass() {
		return Enderecos.class;
	}
}
