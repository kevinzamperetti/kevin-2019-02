package br.com.dbccompany.bancodigital.Dao;

import java.util.ArrayList;
import java.util.List;

import br.com.dbccompany.bancodigital.Dto.AgenciasDTO;
import br.com.dbccompany.bancodigital.Dto.ClientesDTO;
import br.com.dbccompany.bancodigital.Dto.CorrentistasDTO;
import br.com.dbccompany.bancodigital.Entity.Agencias;
import br.com.dbccompany.bancodigital.Entity.Clientes;
import br.com.dbccompany.bancodigital.Entity.Correntistas;

public class CorrentistasDAO extends AbstractDAO<Correntistas>{

	private static final AgenciasDAO AGENCIAS_DAO = new AgenciasDAO(); 
	private static final ClientesDAO CLIENTES_DAO = new ClientesDAO(); 
	
	public Correntistas parseFrom( CorrentistasDTO dto ) {
		Correntistas correntistas = null;
		if ( dto.getIdCorrentista() != null ) {
			correntistas = buscar( dto.getIdCorrentista() );
		} else {
			correntistas = new Correntistas();
		}
		
		correntistas.setRazaoSocial( dto.getRazaoSocial() );
		correntistas.setCnpj( dto.getCnpj() );
		correntistas.setSaldo( dto.getSaldo() );
		correntistas.setTipo( dto.getTipo() );
		
		List<Agencias> agencias = new ArrayList<>();
		for (AgenciasDTO agenciasDTO : dto.getAgencias() ) {
			agencias.add( AGENCIAS_DAO.parseFrom( agenciasDTO ) );
		}
		correntistas.setAgencias( agencias );

		List<Clientes> clientes = new ArrayList<>();
		for (ClientesDTO clientesDTO : dto.getClientes() ) {
			clientes.add( CLIENTES_DAO.parseFrom( clientesDTO ) );
		}
		correntistas.setCorrentistas_clientes( clientes );		
		
		return correntistas;
	}
	
	@Override
	protected Class<Correntistas> getEntityClass() {
		return Correntistas.class;
	}
}
