package br.com.dbccompany.bancodigital.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.Dao.BairrosDAO;
import br.com.dbccompany.bancodigital.Dto.BairrosDTO;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;
import br.com.dbccompany.bancodigital.Entity.Bairros;

public class BairrosService {

	private static final BairrosDAO BAIRROS_DAO = new BairrosDAO(); 
	private static final Logger LOG = Logger.getLogger( BairrosService.class.getName() );
	
	public void salvarBairros( BairrosDTO bairrosDto ) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction  = HibernateUtil.getSession().getTransaction();
		
		Bairros bairros = BAIRROS_DAO.parseFrom( bairrosDto );
		
		try {
			Bairros bairrosRes = BAIRROS_DAO.buscar( "nome", bairros.getNome() );
			if ( bairrosRes == null ) {
				BAIRROS_DAO.criar( bairros );				
			} else {
				bairros.setId( bairrosRes.getId() );
				BAIRROS_DAO.atualizar( bairros );
			}

			if ( started ) {
				transaction.commit();
			}
			
			bairrosDto.setIdBairro( bairros.getId() );
			
		} catch (Exception e) {
			transaction.rollback();
			LOG.log( Level.SEVERE, e.getMessage(), e );
		}
	
	}
}
