package br.com.dbccompany.bancodigital.Dto;

public class EstadosDTO {

	private Integer idEstado;
	private String nome;
	private PaisesDTO paises;

	public Integer getIdEstado() {
		return idEstado;
	}

	public void setIdEstado(Integer idEstado) {
		this.idEstado = idEstado;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public PaisesDTO getPaises() {
		return paises;
	}

	public void setPaises(PaisesDTO paises) {
		this.paises = paises;
	}
	
	
}
