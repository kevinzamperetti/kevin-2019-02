package br.com.dbccompany.bancodigital;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import br.com.dbccompany.bancodigital.Dto.AgenciasDTO;
import br.com.dbccompany.bancodigital.Dto.BairrosDTO;
import br.com.dbccompany.bancodigital.Dto.BancosDTO;
import br.com.dbccompany.bancodigital.Dto.CidadesDTO;
import br.com.dbccompany.bancodigital.Dto.ClientesDTO;
import br.com.dbccompany.bancodigital.Dto.CorrentistasDTO;
import br.com.dbccompany.bancodigital.Dto.EnderecosDTO;
import br.com.dbccompany.bancodigital.Dto.EstadosDTO;
import br.com.dbccompany.bancodigital.Dto.PaisesDTO;
import br.com.dbccompany.bancodigital.Enum.EstadoCivil;
import br.com.dbccompany.bancodigital.Enum.TiposCorrentistas;
import br.com.dbccompany.bancodigital.Service.AgenciasService;
import br.com.dbccompany.bancodigital.Service.BairrosService;
import br.com.dbccompany.bancodigital.Service.BancosService;
import br.com.dbccompany.bancodigital.Service.CidadesService;
import br.com.dbccompany.bancodigital.Service.ClientesService;
import br.com.dbccompany.bancodigital.Service.CorrentistasService;
import br.com.dbccompany.bancodigital.Service.EnderecosService;
import br.com.dbccompany.bancodigital.Service.EstadosService;
import br.com.dbccompany.bancodigital.Service.PaisesService;

public class Main {
	
	private static final Logger LOG = Logger.getLogger(Main.class.getName());
	
	public static void main(String[] args) {

		//Paises
		PaisesService servicePaises = new PaisesService();
		PaisesDTO paisBrasil = new PaisesDTO();
		paisBrasil.setNome( "Brasil" );
		servicePaises.salvarPaises( paisBrasil );
		
		paisBrasil.setNome( "Brasil2" );
		servicePaises.salvarPaises( paisBrasil );
				
		//Estados
		EstadosService serviceEstados = new EstadosService();
		EstadosDTO estadoRS = new EstadosDTO();
		estadoRS.setNome( "Rio Grande do Sul" );
		estadoRS.setPaises( paisBrasil );
		serviceEstados.salvarEstados( estadoRS );

		//Cidades
		CidadesService serviceCidades = new CidadesService();
		CidadesDTO cidadePOA = new CidadesDTO();
		cidadePOA.setNome( "Porto Alegre" );
		cidadePOA.setEstados( estadoRS );
		serviceCidades.salvarCidades( cidadePOA );
		
		//Bairros
		BairrosService serviceBairros = new BairrosService();
		BairrosDTO bairroPassoDaAreia = new BairrosDTO();
		bairroPassoDaAreia.setNome( "Porto Alegre" );
		bairroPassoDaAreia.setCidades( cidadePOA );
		serviceBairros.salvarBairros( bairroPassoDaAreia );		
		
		//Endere�os
		EnderecosService serviceEnderecos = new EnderecosService();
		EnderecosDTO enderecoRuaAndarai = new EnderecosDTO();
		enderecoRuaAndarai.setLogradouro( "Rua Andara�" );
		enderecoRuaAndarai.setNumero( "531" );
		enderecoRuaAndarai.setBairros( bairroPassoDaAreia );
		serviceEnderecos.salvarEnderecos( enderecoRuaAndarai );				
		
		//Clientes
		ClientesService serviceClientes = new ClientesService();
		ClientesDTO clienteJoao = new ClientesDTO();
		clienteJoao.setCpf( "027.000.111-22" );
		clienteJoao.setNome( "Jo�o Batista" );
		clienteJoao.setRg( "20933344455" );
		clienteJoao.setConjuge( "Maria Batista" );
		clienteJoao.setDataNascimento( "12/05/1980" );
		clienteJoao.setEstadoCivil( EstadoCivil.CASADO );
		clienteJoao.setEnderecos( enderecoRuaAndarai );
		serviceClientes.salvarClientes( clienteJoao );
		
		ClientesDTO clienteMaria = new ClientesDTO();
		clienteMaria.setCpf( "027.999.101-02" );
		clienteMaria.setNome( "Maria Batista" );
		clienteMaria.setRg( "20966677788" );
		clienteMaria.setConjuge( "Jo�o Batista" );		
		clienteMaria.setDataNascimento( "29/05/1978" );
		clienteMaria.setEstadoCivil( EstadoCivil.CASADO );
		clienteMaria.setEnderecos( enderecoRuaAndarai );
		serviceClientes.salvarClientes( clienteMaria );		

		//Bancos
		BancosService serviceBancos = new BancosService();
		BancosDTO bancoInter = new BancosDTO();
		bancoInter.setCodigo(1);
		bancoInter.setNome( "Banco Inter" );
		serviceBancos.salvarBancos( bancoInter );
		
		BancosDTO bancoNuBank = new BancosDTO();
		bancoNuBank.setCodigo(1);
		bancoNuBank.setNome( "Nu Bank" );
		serviceBancos.salvarBancos( bancoNuBank );
		
		//Agencias
		AgenciasService serviceAgencias = new AgenciasService();
		AgenciasDTO agenciaNuBank1 = new AgenciasDTO();
		agenciaNuBank1.setCodigo(1);
		agenciaNuBank1.setNome( "Agencia 1" );
		agenciaNuBank1.setBancos( bancoNuBank );
		serviceAgencias.salvarAgencias( agenciaNuBank1 );		

		List<AgenciasDTO> agenciaLista = new ArrayList<>();
		agenciaLista.add( agenciaNuBank1 );

		//Correntistas
		CorrentistasService serviceCorrentistas = new CorrentistasService();
		
		CorrentistasDTO correntistaJoao = new CorrentistasDTO();
		correntistaJoao.setAgencias( agenciaLista );
		correntistaJoao.setTipo( TiposCorrentistas.PF );
		correntistaJoao.depositar( 100 );
		System.out.println("Deposito realizado. Saldo Jo�o: " + correntistaJoao.getSaldo());
		serviceCorrentistas.salvarCorrentistas( correntistaJoao );
		
		CorrentistasDTO correntistaMaria = new CorrentistasDTO();
		correntistaMaria.setAgencias( agenciaLista );
		correntistaMaria.setTipo( TiposCorrentistas.PF );
		correntistaJoao.transferir( 50 , correntistaMaria);
		
		serviceCorrentistas.salvarCorrentistas( correntistaJoao );
		serviceCorrentistas.salvarCorrentistas( correntistaMaria );
		System.out.println("Saldo Maria: " + correntistaMaria.getSaldo());			
		
		System.exit(0);  //sucesso
	}
}	

/*		//Paises sem DTO
		PaisesService servicePaises = new PaisesService();
		Paises paises = new Paises();
		paises.setNome("Argentina");
		servicePaises.salvarPaises(paises);
		System.exit(0);  //sucesso
*/
	
/*	public static void main(String[] args) {
		Session session = null;
		Transaction transaction = null;
		try {
			session = HibernateUtil.getSession();
			transaction = session.beginTransaction();
			
			Paises paises = new Paises();
			paises.setNome("Brasil");
			session.save(paises);
			
			//session.createQuery("select * from paises;").executeUpdate();
			
			Criteria criteria = session.createCriteria(Paises.class);
			criteria.createAlias("nome", "nome_paises");
			criteria.add(
					Restrictions.isNotNull("nome")
					
			);
			
			List<Paises> lstPaises = criteria.list();
			
			transaction.commit();
			
		} catch (Exception e) {
			if ( transaction != null ) {
				transaction.rollback();
			}
			LOG.log(Level.SEVERE, e.getMessage(), e);
			System.exit(1);  //erro
			
		} finally {
			if ( session != null ) {
				session.close();
			}
		}
		System.exit(0);  //sucesso
	}*/

