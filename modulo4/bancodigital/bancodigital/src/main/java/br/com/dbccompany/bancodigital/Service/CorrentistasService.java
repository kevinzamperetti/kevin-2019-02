package br.com.dbccompany.bancodigital.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.Dao.CorrentistasDAO;
import br.com.dbccompany.bancodigital.Dto.CorrentistasDTO;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;
import br.com.dbccompany.bancodigital.Entity.Correntistas;

public class CorrentistasService {

	private static final CorrentistasDAO CORRENTISTAS_DAO = new CorrentistasDAO(); 
	private static final Logger LOG = Logger.getLogger( CorrentistasService.class.getName() );
	
	public void salvarCorrentistas( CorrentistasDTO correntistasDTO ) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction  = HibernateUtil.getSession().getTransaction();
		
		Correntistas correntistas = CORRENTISTAS_DAO.parseFrom( correntistasDTO );
		
		try {
			Correntistas correntistasRes = CORRENTISTAS_DAO.buscar( correntistas.getId() );
			if ( correntistasRes == null ) {
				CORRENTISTAS_DAO.criar( correntistas );				
			} else {
				correntistas.setId( correntistasRes.getId() );
				CORRENTISTAS_DAO.atualizar( correntistas );
			}

			if ( started ) {
				transaction.commit();
			}
			
			correntistasDTO.setIdCorrentista( correntistas.getId() );
			
		} catch (Exception e) {
			transaction.rollback();
			LOG.log( Level.SEVERE, e.getMessage(), e );
		}
	
	}
}
