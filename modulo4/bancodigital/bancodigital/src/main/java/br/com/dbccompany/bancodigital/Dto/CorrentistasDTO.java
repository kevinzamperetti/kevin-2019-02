package br.com.dbccompany.bancodigital.Dto;

import java.util.ArrayList;
import java.util.List;

import br.com.dbccompany.bancodigital.Enum.TiposCorrentistas;

public class CorrentistasDTO {

	private Integer idCorrentista;
	private String razaoSocial;
	private String cnpj;
	private double saldo;
	private TiposCorrentistas tipo;
	private List<ClientesDTO> clientes = new ArrayList<>();	
	private List<AgenciasDTO> agencias = new ArrayList<>();
	
	public Integer getIdCorrentista() {
		return idCorrentista;
	}
	
	public void setIdCorrentista(Integer idCorrentista) {
		this.idCorrentista = idCorrentista;
	}
	
	public String getRazaoSocial() {
		return razaoSocial;
	}
	
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	
	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public String getCnpj() {
		return cnpj;
	}
	
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	
	public TiposCorrentistas getTipo() {
		return tipo;
	}
	
	public void setTipo(TiposCorrentistas tipo) {
		this.tipo = tipo;
	}
	
	public List<ClientesDTO> getClientes() {
		return clientes;
	}

	public void setClientes(List<ClientesDTO> clientes) {
		this.clientes = clientes;
	}

	public List<AgenciasDTO> getAgencias() {
		return agencias;
	}

	public void setAgencias(List<AgenciasDTO> agencias) {
		this.agencias = agencias;
	}

	public boolean depositar( double valor ) {
		if ( valor >0 ) {
			this.saldo = this.saldo + valor;
			return true;
		}
		return false;
	}
	
	public boolean sacar( double valor ) {
		if ( this.saldo >= valor ) {
			this.saldo = this.saldo - valor;
			return true; 
		}
		return false;
	}
	
	public boolean transferir( double valor, CorrentistasDTO correntistaDestino ) {
		if ( valor > 0 ) {
			if ( this.sacar( valor ) ) {
				correntistaDestino.depositar( valor );
				return true;
			}
		}
		return false;
	}
}
