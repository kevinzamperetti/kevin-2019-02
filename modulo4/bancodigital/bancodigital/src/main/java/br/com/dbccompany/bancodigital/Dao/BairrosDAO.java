package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.BairrosDTO;
import br.com.dbccompany.bancodigital.Entity.Bairros;
import br.com.dbccompany.bancodigital.Entity.Cidades;

public class BairrosDAO extends AbstractDAO<Bairros>{

	private static final CidadesDAO CIDADES_DAO = new CidadesDAO(); 

	public Bairros parseFrom( BairrosDTO dto ) {
		Bairros bairros = null;
		if ( dto.getIdBairro() != null ) {
			bairros = buscar( dto.getIdBairro() );
		} else {
			bairros = new Bairros();
		}
		
		Cidades cidade = CIDADES_DAO.parseFrom( dto.getCidades() );
		
		bairros.setNome( dto.getNome() );
		bairros.setCidade( cidade );
		return bairros;
	}	
	
	
	@Override
	protected Class<Bairros> getEntityClass() {
		return Bairros.class;
	}
}
