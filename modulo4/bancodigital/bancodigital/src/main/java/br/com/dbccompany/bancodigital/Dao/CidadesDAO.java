package br.com.dbccompany.bancodigital.Dao;

import br.com.dbccompany.bancodigital.Dto.CidadesDTO;
import br.com.dbccompany.bancodigital.Entity.Cidades;
import br.com.dbccompany.bancodigital.Entity.Estados;

public class CidadesDAO extends AbstractDAO<Cidades>{
	
	private static final EstadosDAO ESTADOS_DAO = new EstadosDAO();
	
	public Cidades parseFrom( CidadesDTO dto ) {
		Cidades cidades = null;
		if ( dto.getIdCidade() != null ) {
			cidades = buscar( dto.getIdCidade() );
		} else {
			cidades = new Cidades();
		}
		
		Estados estado = ESTADOS_DAO.parseFrom( dto.getEstados() );
		
		cidades.setNome( dto.getNome() );
		cidades.setEstado(estado);
		return cidades;
	}	

	@Override
	protected Class<Cidades> getEntityClass() {
		return Cidades.class;
	}
}
