package br.com.dbccompany.vemserSpring.Entity;

import br.com.dbccompany.vemserSpring.Enum.Status;
import br.com.dbccompany.vemserSpring.Enum.TipoPersonagem;

import javax.persistence.*;

@Entity
@Inheritance( strategy = InheritanceType.SINGLE_TABLE )
@SequenceGenerator( allocationSize = 1, name = "PERSONAGENS_SEQ", sequenceName = "PERSONAGENS_SEQ" )
public class Personagem {

    @Id
    @Column( name = "id_inventario" )
    @GeneratedValue( generator = "PERSONAGENS_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;
    private String nome;
    private Status status;
    private double qtdVida;
    private int experiencia;
    private int qtdExperienciaPorAtaque;
    private double dano;

    @OneToOne( mappedBy = "personagem")
    private Inventario inventario;

    @Enumerated( EnumType.STRING )
    private TipoPersonagem tipoPersonagem;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public double getQtdVida() {
        return qtdVida;
    }

    public void setQtdVida(double qtdVida) {
        this.qtdVida = qtdVida;
    }

    public int getExperiencia() {
        return experiencia;
    }

    public void setExperiencia(int experiencia) {
        this.experiencia = experiencia;
    }

    public int getQtdExperienciaPorAtaque() {
        return qtdExperienciaPorAtaque;
    }

    public void setQtdExperienciaPorAtaque(int qtdExperienciaPorAtaque) {
        this.qtdExperienciaPorAtaque = qtdExperienciaPorAtaque;
    }

    public double getDano() {
        return dano;
    }

    public void setDano(double dano) {
        this.dano = dano;
    }

    public TipoPersonagem getTipoPersonagem() {
        return tipoPersonagem;
    }

    protected void setTipoPersonagem(TipoPersonagem tipoPersonagem) {
        this.tipoPersonagem = tipoPersonagem;
    }

    public Inventario getInventario() {
        return inventario;
    }

    public void setInventario(Inventario inventario) {
        this.inventario = inventario;
    }
}
