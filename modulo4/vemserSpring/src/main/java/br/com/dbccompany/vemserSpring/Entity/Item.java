package br.com.dbccompany.vemserSpring.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@SequenceGenerator( allocationSize = 1, name = "ITENS_SEQ", sequenceName = "ITENS_SEQ" )
public class Item {

    @Id
    @Column( name = "id_item" )
    @GeneratedValue( generator = "ITENS_SEQ", strategy = GenerationType.SEQUENCE )
    private Integer id;
    private String descricao;

    @OneToMany ( mappedBy = "idItem", cascade = CascadeType.ALL)
    private List<InventarioXItem> listaItensInventarios = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public List<InventarioXItem> getListaItensInventarios() {
        return listaItensInventarios;
    }

    public void setListaItensInventarios(List<InventarioXItem> listaItensInventarios) {
        this.listaItensInventarios = listaItensInventarios;
    }
}
