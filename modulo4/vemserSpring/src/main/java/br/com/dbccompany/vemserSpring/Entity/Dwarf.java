package br.com.dbccompany.vemserSpring.Entity;

import br.com.dbccompany.vemserSpring.Enum.TipoPersonagem;

import javax.persistence.Entity;

@Entity
public class Dwarf extends Personagem {

    public Dwarf() {
        super.setTipoPersonagem( TipoPersonagem.DWARF );
    }
}
