package br.com.dbccompany.vemserSpring.Entity;

import br.com.dbccompany.vemserSpring.Enum.TipoPersonagem;

import javax.persistence.Entity;

@Entity
public class Elfo extends Personagem {

    public Elfo() {
        super.setTipoPersonagem( TipoPersonagem.ELFO );
    }
}
