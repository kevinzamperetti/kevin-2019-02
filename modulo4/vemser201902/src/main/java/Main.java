import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

    public static void main(String[] args) {
        Connection conn = Connector.connect();
        try {
            ResultSet rs = conn.prepareStatement("select tname from tab where tname = 'PAISES'").executeQuery();
            if (!rs.next()) {
                conn.prepareStatement("CREATE TABLE PAISES(\n"
                        + " ID_PAIS INTEGER NOT NULL PRIMARY KEY,\n"
                        + " NOME VARCHAR(100) NOT NULL\n"
                        + ")").execute();
            }

            PreparedStatement pst = conn.prepareStatement("INSERT INTO PAISES(ID_PAIS, NOME) "
                                + "VALUES(PAISES_SEQ.NEXTVAL, ?)");
            pst.setString(1, "Brasil");
            pst.executeUpdate();

            rs = conn.prepareStatement("SELECT * FROM PAISES").executeQuery();
            while (rs.next()) {
                System.out.println(String.format("Nome do Pais: %s", rs.getString("NOME")));
            }

            /*ESTADOS*/
            ResultSet rs_estados = conn.prepareStatement("select tname from tab where tname = 'ESTADOS'").executeQuery();
            if (!rs_estados.next()) {
                conn.prepareStatement("CREATE TABLE ESTADOS(\n" +
                        "    ID_ESTADO INTEGER NOT NULL PRIMARY KEY,\n" +
                        "    NOME VARCHAR2(100) NOT NULL,\n" +
                        "    FK_ID_PAIS INTEGER NOT NULL,\n" +
                        "    FOREIGN KEY(FK_ID_PAIS)\n" +
                        "        REFERENCES PAISES(ID_PAIS)\n" +
                        ")").execute();
            }

            PreparedStatement pst_estados = conn.prepareStatement("INSERT INTO ESTADOS(ID_ESTADO, NOME, FK_ID_PAIS) \n" +
                                " VALUES(ESTADOS_SEQ.NEXTVAL, ?, 1 )");
            pst_estados.setString(1, "NA");
            pst_estados.executeUpdate();

            rs_estados = conn.prepareStatement("SELECT * FROM ESTADOS").executeQuery();
            while (rs_estados.next()) {
                System.out.println(String.format("Nome do Estado: %s", rs_estados.getString("NOME")));
            }

            /*CIDADES*/
            ResultSet rs_cidades = conn.prepareStatement("select tname from tab where tname = 'CIDADES'").executeQuery();
            if (!rs_cidades.next()) {
                conn.prepareStatement("CREATE TABLE CIDADES(\n" +
                        "    ID_CIDADE INTEGER NOT NULL PRIMARY KEY,\n" +
                        "    NOME VARCHAR2(100) NOT NULL,\n" +
                        "    FK_ID_ESTADO INTEGER NOT NULL,\n" +
                        "    FOREIGN KEY(FK_ID_ESTADO)\n" +
                        "        REFERENCES ESTADOS(ID_ESTADO)\n" +
                        ")").execute();
            }

            PreparedStatement pst_cidades = conn.prepareStatement("INSERT INTO CIDADES(ID_CIDADE, NOME, FK_ID_ESTADO) \n" +
                                " VALUES(CIDADES_SEQ.NEXTVAL, ?, 1 )");
            pst_cidades.setString(1, "NA");
            pst_cidades.executeUpdate();

            rs_cidades = conn.prepareStatement("SELECT * FROM CIDADES").executeQuery();
            while (rs_cidades.next()) {
                System.out.println(String.format("Nome da Cidade: %s", rs_cidades.getString("NOME")));
            }

            /*BAIRROS*/
            ResultSet rs_bairros = conn.prepareStatement("select tname from tab where tname = 'BAIRROS'").executeQuery();
            if (!rs_bairros.next()) {
                conn.prepareStatement("CREATE TABLE BAIRROS(\n" +
                        "    ID_BAIRRO INTEGER NOT NULL PRIMARY KEY,\n" +
                        "    NOME VARCHAR2(100) NOT NULL,\n" +
                        "    FK_ID_CIDADE INTEGER NOT NULL,\n" +
                        "    FOREIGN KEY(FK_ID_CIDADE)\n" +
                        "        REFERENCES CIDADES(ID_CIDADE)\n" +
                        ")").execute();
            }

            PreparedStatement pst_bairros = conn.prepareStatement("INSERT INTO BAIRROS(ID_BAIRRO, NOME, FK_ID_CIDADE) \n" +
                    "             VALUES(BAIRROS_SEQ.NEXTVAL, ?, 1)");
            pst_bairros.setString(1, "NA");
            pst_bairros.executeUpdate();

            rs_bairros = conn.prepareStatement("SELECT * FROM BAIRROS").executeQuery();
            while (rs_bairros.next()) {
                System.out.println(String.format("Nome da Bairro: %s", rs_bairros.getString("NOME")));
            }

        } catch (SQLException ex) {
            Logger.getLogger(Connector.class.getName()).log(Level.SEVERE, "ERRO na Consulta do Main", ex);
        }
    }

}
