import React from 'react';
import { Link } from 'react-router-dom'

const MenuPrincipal = props => {

  function logout() {
    localStorage.removeItem('Authorization')
  }

  return (
    <React.Fragment>
      <section className="menu-principal">
        <Link to="/" >Pagina Inicial</Link>
        <Link to="/elfo">Cadastrar Elfo</Link>
        <Link to="/listar-elfos">Listar Elfos</Link>
        <Link type="button" onClick={ logout.bind( this ) } href="/login">Logout</Link>
      </section>
    </React.Fragment>
  )
}

export default MenuPrincipal;