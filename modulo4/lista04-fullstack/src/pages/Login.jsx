import React, { Component } from 'react'
import '../App.css';
import api from '../services/api';

export default class Login extends Component {
	constructor( props ){
		super( props )
		this.state = {
			username: '',
			password: ''
		}
		this.trocaValoresState = this.trocaValoresState.bind( this )
	}

	trocaValoresState( evt ) {
		const { name, value } = evt.target
		this.setState( {
			[name]: value
		} )
	}

	logar( evt ) {
		evt.preventDefault();
		const { username, password } = this.state
		if ( username && password ){
			api.post( '/login', {
				username: this.state.username,
				password: this.state.password
			} ).then( resp => {
				localStorage.setItem('Authorization', resp.headers.authorization);
				this.props.history.push( "/" )
			} )
		}
	}

	render() {
		return  (
			<React.Fragment>
				<strong className="login" >Efetue login para acessar o sistema</strong>
				<div className="div-login">
					<span>E-mail: </span>
					<input className="input-login" type="text" name="username" id="username" placeholder="Digite seu username" onChange={this.trocaValoresState} />
					<span>Senha: </span>
					<input className="input-login" type="password" name="password" id="password" placeholder="Digite sua password" onChange={this.trocaValoresState} />
					<button className="button-login" type="button" onClick={ this.logar.bind( this ) } >Login</button>
				</div>
			</React.Fragment>
		)
	}
}
