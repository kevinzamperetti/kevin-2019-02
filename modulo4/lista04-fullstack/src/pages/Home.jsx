import React, { Component } from 'react';
import MenuPrincipal from '../components/MenuPrincipal'
import {Link } from 'react-router-dom'
import '../App.css'

class Home extends Component {
  constructor( props ) {
    super( props )
    this.props = props
  }  

  render() {

    return (
      <React.Fragment>
        <MenuPrincipal/>

        <div className="lista">
          <article>
            <h1>Cadastrar Elfo</h1>
            <Link to={ `/elfo` }>Cadastrar</Link>
          </article>
        </div>

        <div className="lista">
          <article>
            <h1>Listar Elfos</h1>
            <Link to={ `/listar-elfos` }>Listar</Link>
          </article>
        </div>              
      </React.Fragment>
    )
  }
}

export default Home;
