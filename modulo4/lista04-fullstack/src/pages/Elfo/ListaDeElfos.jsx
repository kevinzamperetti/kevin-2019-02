import React, { Component } from 'react';
import MenuPrincipal from '../../components/MenuPrincipal'
import api from '../../services/api'
import {Link } from 'react-router-dom'
import InputFiltro from '../../components/InputFiltro';

export default class ListaDeElfos extends Component {
  constructor( props ) {
    super( props )
    this.state = {
      elfos: []
    }
  }

  //Ação logo que o componente é exibido em tela
  componentDidMount() { 
    this.carregaListaDeElfos()
  }

  carregaListaDeElfos = async () => {
    const header = { headers: {Authorization: localStorage.getItem('Authorization') } }
    const response = await api.get( '/api/elfo/', header )
    this.setState( { elfos: response.data }  )
  }

  filtrar( evt ) {
    const elfo = this.state.elfos
    const pesquisa = evt.target.value
    if ( pesquisa ) {
      const filtro = elfo.filter( elfo => elfo.nome.toLowerCase().includes( pesquisa.toLowerCase() ) )
      this.setState( { elfos: filtro } )
    } else {
      this.carregaListaDeElfos()
    }
  }

  render() {
    const { elfos } = this.state
    return (
      <React.Fragment>
        <MenuPrincipal/>
        <InputFiltro filtrar={ this.filtrar.bind( this ) } placeholder="Pesquisar Elfos pelo nome"/>
        <h1>Total de Elfos: { this.state.elfos.length } </h1>
        <div className="lista">
          { elfos.map( elfo => (
            <article key={ elfo.id }>
              <strong>Nome: { elfo.nome }</strong> <br/>
              <strong>Vida: { elfo.qtdVida }</strong> <br/>
              <strong>Status: { elfo.status }</strong> <br/>
              <strong>Raça: { elfo.tipoPersonagem }</strong> <br/>
              {/* <Link to={ `/elfo/${ elfo.id}` }>Detalhes</Link> */}
            </article>
            ) )
          }
        </div>
      </React.Fragment>
    )
  }
}
