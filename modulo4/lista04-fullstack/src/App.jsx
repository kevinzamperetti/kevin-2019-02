/* eslint-disable no-useless-constructor */
import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom'
import { PrivateRoute } from './components/PrivateRoute'
import './App.css';

import Login from './pages/Login'
import Home from './pages/Home'
import ListaDeElfos from './pages/Elfo/ListaDeElfos';
import Elfo from './pages/Elfo/Elfo';

class App extends Component {
  constructor( props ) {
    super( props )
  }

  render() {
    return (
        <Router>
          <section className="App">
            <PrivateRoute exact path="/" component={ Home } />
            <Route path="/login" component={ Login } />
            <PrivateRoute path="/listar-elfos" component={ ListaDeElfos } />
            <PrivateRoute path="/elfo" component={ Elfo } />
          </section>
        </Router>
      
    )
  }
}

export default App;
