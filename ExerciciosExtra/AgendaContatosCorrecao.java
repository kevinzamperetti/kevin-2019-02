import java.util.*;

public class AgendaContatosCorrecao{
    private HashMap<String, String> contatos;
    
    public AgendaContatosCorrecao(){
        contatos = new LinkedHashMap<>();
    }
    
    public void adicionar(String nome, String telefone){
        contatos.put(nome, telefone);
    }
    
    public String consultar(String nome){
        return contatos.get(nome);
    }
    
    //entrySet: retorna um conjunto de Maps contido no mapa, podendo acessar suas chaves e valores.    
    public String consultarNome(String telefone){
        for(HashMap.Entry<String, String> par : contatos.entrySet()) { //basicamente cria um hash invertido
            String chave = par.getKey();
            String valor = par.getValue();
            if(valor.equals(telefone)){  //se valor do hash igual ao telefone retorna o nome
                return chave;
            }
        }
        return null;
    }
    
    public String csvX(){
        StringBuilder builder = new StringBuilder();
        String separador = System.lineSeparator();
        
        for(HashMap.Entry<String, String> par : contatos.entrySet()) { //basicamente cria um hash invertido
            String chave = par.getKey();
            String valor = par.getValue();
            String contato = String.format("%s,%s%s", chave, valor, separador);
            builder.append(contato);
        }
        return builder.toString();
    }
    


    
}
