public class TradutorParaBosnio implements Tradutor{
    public String traduzir(String textoEmPortugues){
        switch(textoEmPortugues){
            case "Sim": 
                return "Yes";
            case "Obrigado": 
            case "Obrigada": 
                return "Hvala";
            default: 
                return null;
        }
    }
}
