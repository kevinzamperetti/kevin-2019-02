import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class AgendaContatosCorrecaoTest{
    
   /* @Test
    public void adicionarContatoEPesquisar(){
    AgendaContatosCorrecao agenda = new AgendaContatosCorrecao();
        agenda.adicionar("Marcos ", "555555 ");
        agenda.adicionar("Mithrandir ", "444444 ");
        agenda.adicionar("Kevin", "999999");   
        assertEquals("444444 ", agenda.consultar("Mithrandir "));
    }
    
    @Test
    public void adicionarContatoEPesquisarPorTelefone(){
    AgendaContatosCorrecao agenda = new AgendaContatosCorrecao();
        agenda.adicionar("Marcos ", "555555 ");
        agenda.adicionar("Mithrandir ", "444444 ");
        agenda.adicionar("Kevin", "999999");   
        assertEquals("Mithrandir ", agenda.consultarNome("444444 "));        
    }    */
    
    @Test
    public void adicionarContatoEGerarCSVX(){
    AgendaContatosCorrecao agenda = new AgendaContatosCorrecao();
        agenda.adicionar("Marcos ","555555 ");
        //agenda.adicionar("Mithrandir ","444444 ");
//        agenda.adicionar("Kevin", "999999");   
        String separador = System.lineSeparator();
        String esperado = String.format("Marcos,555555%s", separador);
        assertEquals(esperado, agenda.csvX());        
    }    
}
