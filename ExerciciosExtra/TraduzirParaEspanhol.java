public class TraduzirParaEspanhol implements Tradutor{
    
    public String traduzir(String textoEmPortugues){
        switch(textoEmPortugues){
            case "Sim": 
                return "Sí";
            case "Obrigado": 
            case "Obrigada": 
                return "Gracias";
            default: 
                return null;
        }
    }
}
