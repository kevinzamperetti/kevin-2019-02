import java.util.*;

public class AgendaContatos{
    private HashMap<String, String> agenda;
    
    public AgendaContatos(){
        agenda = new LinkedHashMap<String, String>();
    }
    
    public void adicionarContato(String nome, String telefone){
        agenda.put(nome, telefone);
    }
    
    public String obterTelefone(String nome){
        return agenda.get(nome);
    }
    
    //entrySet: retorna um conjunto de Maps contido no mapa, podendo acessar suas chaves e valores.    
    public String pesquisarContato(String telefone){
        for(Map.Entry<String, String> entry : agenda.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            if(value.equals(telefone)){  //se valor do hash igual ao telefone retorna o nome
                return key;
            }
        }
        return null;
    }
    
    
    public String csv(){
        StringBuilder builder = new StringBuilder();
        String separador = System.lineSeparator();
        for(HashMap.Entry<String, String> par : agenda.entrySet()) { //basicamente cria um hash invertido
            String chave = par.getKey();
            String valor = par.getValue();
            String contato = String.format("%s,%s%s", chave, valor, separador);
            builder.append(contato);
        }
        return builder.toString();
    }


    
}
