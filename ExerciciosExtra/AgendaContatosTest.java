import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class AgendaContatosTest{
    @Test
    public void adicionarContatoEPesquisar(){
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionarContato("Marcos ", "555555 ");
        agenda.adicionarContato("Mithrandir ", "444444 ");
        agenda.adicionarContato("Kevin", "999999");
        
        assertEquals("444444 ", agenda.obterTelefone("Mithrandir "));
        assertEquals("Mithrandir ", agenda.pesquisarContato("444444 "));
    }
    
    @Test
    public void adicionarContatoEGerarCSV(){
    AgendaContatos agenda = new AgendaContatos();
        agenda.adicionarContato("Marcos ", "555555 ");
        agenda.adicionarContato("Mithrandir ", "444444 ");
        agenda.adicionarContato("Kevin", "999999");   
        String separador = System.lineSeparator();
        String esperado = String.format("Marcos,555555%sMithrandir,444444%sKevin,999999", separador, separador, separador);
        assertEquals(esperado, agenda.csv());        
    }
    
}
