//Exercício 1
function calcularCirculo({raio, tipoCalculo:tipo}){
    return Math.ceil(tipo == "A" ? Math.PI * Math.pow(raio, 2) : 2 * Math.PI * raio);
    // Math.ceil arredonda pra cima
}

let circulo = {
    raio: 3,
    tipoCalculo: "A"
}

//console.log(calcularCirculo(circulo));

// Exercício 2
function naoBissexto(ano) {
    return ((ano % 400 === 0) || (ano % 4 === 0 && ano % 100 !== 0)) ? false : true;
}

// console.log(naoBissexto(2016));
// console.log(naoBissexto(2017));

//let naoBissextoNovo = ano => (ano % 400 === 0) || (ano % 4 === 0 && ano % 100 !== 0) ? false : true;
//console.log(naoBissextoNovo(2016));
//console.log(naoBissextoNovo(2017));

const testes = {
    diaAula: "Segunda",
    local: "DBC",
    naoBissexto(ano) {
        return ((ano % 400 === 0) || (ano % 4 === 0 && ano % 100 !== 0)) ? false : true;
    }
}

//console.log(testes.naoBissexto(2016));

// Exercício 3
function somaPares(numeros) {
    let resultado =0;
    for (let i = 0; i < numeros.length; i++) {
        if(i % 2 === 0) {
            resultado += numeros[i];
        }
    }
    return resultado;
}

//console.log(somaPares([ 1, 56, 4.34, 6, -2 ]));

// Exercício 4
/*function adicionar(n1) {
    return function (n2) {
        return n1 + n2;
    }
}*/

let adicionar = op1 => op2 => op1 + op2;

//console.log(adicionar(3)(4)); // 7
//console.log(adicionar(5642)(8749)); // 14391

/*const is_divisivel = (divisor, numero) => !(numero % divisor);
const divisor = 2;
console.log(is_divisivel(divisor, 20 ));
console.log(is_divisivel(divisor, 11 ));
console.log(is_divisivel(divisor, 12));*/

const divisivelPor = divisor => numero => !(numero % divisor);

const is_divisivel = divisivelPor(2);
const is_divisivel3 = divisivelPor(3);

// console.log(is_divisivel(20));
// console.log(is_divisivel(11));
// console.log(is_divisivel(12));
// console.log(is_divisivel3(20));
// console.log(is_divisivel3(11));
// console.log(is_divisivel3(12));

// Exercício 5
function arredondar(numero, precisao = 2) {
    const fator = Math.pow(10, precisao);
    return Math.ceil( numero * fator) / fator;
}

function imprimirBRL(numero) {
    let qtdCasasMilhares = 3;
    let separadorMilhar = ".";
    let separadorDecimal = ",";

    let stringBuffer = [];
    let parteDecimal = arredondar(Math.abs(numero)%1);  //pega numero absoluto
    let parteInteira = Math.trunc(numero); //pegar somente parte inteira
    let parteInteiraString = Math.abs(parteInteira).toString();
    let parteInteiraTamanho = parteInteiraString.length;

    let c = 1;
    while (parteInteiraString.length > 0) {
        if (c % qtdCasasMilhares == 0) {
            stringBuffer.push(`${separadorMilhar}${parteInteiraString.slice(parteInteiraTamanho - c)}`);  //push adiciona no array
            parteInteiraString = parteInteiraString.slice(0, parteInteiraTamanho - c);
        } else if (parteInteiraString.length < qtdCasasMilhares) {
            stringBuffer.push(parteInteiraString);
            parteInteiraString = "";
        }
        c++;
    }
    stringBuffer.push(parteInteiraString);
    
    let decimalString = parteDecimal.toString().replace('0.', '').padStart(2, '0');
    return `${parteInteira >= 0 ? 'R$' : '-R$'}${stringBuffer.reverse().join('')}${separadorDecimal}${decimalString}`;
}

// console.log(imprimirBRL(0)); // “R$ 0,00”
// console.log(imprimirBRL(3498.99)); // “R$ 3.498,99”
// console.log(imprimirBRL(-3498.99)); // “-R$ 3.498,99”
// console.log(imprimirBRL(2313477.0135)); // “R$ 2.313.477,02”

