  /*
  class Turma {
    constructor(ano) {
      this.ano = ano;
    }

    apresentarAno() {
      console.log(`Essa turma é do ano de: ${this.ano}`);
    }

    static info() {  //só ser acessado dentro de nossa classe
      console.log(`Testando informações`)
    }

    get anoTurma() {
      console.log(this.ano);
    }

    set localMetodo(localizacao) {
      this.localTurma = localizacao;
    }

  }
  */

  /*
  const vemSer = new Turma("2019/02");

  vemSer.apresentarAno();

  // vemSer.info(); gera erro porque não é acessível fora da classe

  vemSer.anoTurma;
  vemSer.local = "DBC";
  console.log(vemSer.localTurma);
  */

  /* 
  class VemSer extends Turma {
    constructor(local, qtdAlunos) {
      super(ano);
      this.local = local;
      this.qtdAlunos = qtdAlunos;
    }

    descricao() {
      console.log(`Turma do Vem Ser ano ${ano}, realizado na ${this.local}, quantidade de alunos ${this.qtdAlunos} `);
    }
  }

  revisar
  const vemSer2 = new VemSer("2019/02", "DBC", 17);
  vemSer2.descricao();
  */

  /*
  let defer = new Promise((resolve, reject) => {  
    setTimeout(() => {  //Exemplo de uso: quando espero resultado de uma API 
      if(true){
        resolve('Foi resolvido');
      }else{
        reject('Erro');
      }
    }, 3000); //espera 3 segundos e depois resolve a promessa
  });

  defer
      .then( (data) => {  //caso seja resolvido
        console.log(data);
        return "Novo resultado";
      })  
      .then( (data) => {
        console.log(data);
      })
      .catch((erro) => {  //para tratar o erro
        console.log(erro)
      }) 
  */

  /*
  let pokemon = fetch(`https://pokeapi.co/api/v2/pokemon/`);
  let pokemon1 = fetch(`https://pokeapi.co/api/v2/pokemon/1`);

  //pending
  //resolved
  //rejected

  pokemon
      .then( data => data.json() )
      .then( data => { console.log(data.results)} )
  */

  let valor1 = new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve({ valorAtual: '1' })
    }, 4000)
  })


  let valor2 = new Promise((resolve, reject) => {
    resolve({ valorAtual: '2' })
  })

/*   Promise
    .all([valor1, valor2])
    .then(resposta => {
      console.log(resposta)
    }) */

Promise
    .race([valor1, valor2])  //traz somente o que resolver primeiro
    .then(resposta => {
      console.log(resposta)
    })