// console.log("Cheguei");

//......variáveis
// console.log(teste1); //gera erro, pois teste1 não está definido

//var teste = 123;      //escopo local
//let teste1 = "1233";  //escopo global
//const teste2 = 1222;  //constante

//var teste = 111;
//teste1 = "222";  
//teste2 = "aqui";

// console.log(teste);  //111
// console.log(teste1); //222
// console.log(teste2); //gera erro, pois o valor é constante

//console.log(teste1); //gera erro, pois o valor é constante

{
    let teste1 = "Aqui mudou";
//    console.log(teste1);
}

//console.log(teste1); //gera erro, pois o valor é constante

//......função e objetos

const pessoa = {
    nome: "Kevin",
    idade: 27,
    endereco: {
        logradouro: "Rua Fernando Cortez",
        numero: 150
    }
}

Object.freeze(pessoa);  //não permite que o objeto pessoa seja alterado

pessoa.nome = "Kevin Zamperetti";  //não altera o nome porque "congelamos" o estado do objeto com freeze

//console.log(pessoa);

function somar(valor1, valor2){
    console.log(valor1 + valor2);
}

//somar(2, 3);

function ondeMoro(cidade){
    //console.log("Eu moro em " + cidade + "e sou muito feliz");  
    //console.log(`Eu moro em ${cidade} e sou muito feliz`);  //template string
}

//ondeMoro("Porto Alegre");

function fruteira(){
    let texto = "Banana"
                + "\n"
                + "Ameixa"
                + "\n"
                + "Goiaba"
                + "\n"
                + "Pêssego"
                + "\n";
    let newTexto = 
`Banana
Ameixa
Goiaba
Pêssego`
                
    console.log(newTexto);
}

//fruteira();

function quemSou(pessoa){
    console.log(`Meu nome é ${pessoa.nome} e tenho ${pessoa.idade} anos`);  //template string
}

//quemSou(pessoa);

let funcaoSomarVar = function(a, b, c = 0){
    return a + b + c;
}

let add = funcaoSomarVar;
let resultado = add(3, 2);
//console.log(resultado);

const { nome:n, idade:i } = pessoa;
const { endereco: { logradouro, numero } } = pessoa;
//console.log(n, i);
//console.log(logradouro, numero);

const array = [1, 3, 4, 8];
//array[] = 1 //se for let ao invés de const

const [ n1, , n2, , n3 = 9 ] = array;
//console.log(n1, n2, n3);

function testarPessoa({ nome, idade }, nomenclatura){
    console.log(nome, idade, nomenclatura);
}
//testarPessoa(pessoa, "teste");

let a1 = 42;
let b1 = 15;

//console.log(a1, b1);
[a1, b1] = [b1, a1];
//console.log(a1, b1);
