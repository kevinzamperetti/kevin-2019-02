function cardapioIFood( veggie = true, comLactose = false ) {
    let cardapio = [
      'enroladinho de salsicha',
      'cuca de uva'
    ]
  
    if ( comLactose ) {
      cardapio.push( 'pastel de queijo' )
    }
   
    cardapio = [...cardapio, 'pastel de carne', 'empada de legumes marabijosa']
    
    /*cardapio = cardapio.concat( [
      'pastel de carne',
      'empada de legumes marabijosa'
    ] )*/
  
    if ( veggie ) {
      // TODO: remover alimentos com carne (é obrigatório usar splice!)
      arr = cardapio.splice( cardapio.indexOf( 'enroladinho de salsicha' ), 1 )
      arr = cardapio.splice( cardapio.indexOf( 'pastel de carne' ), 1 )
    }
    
    /*filtro dentro do array
      let resultado = cardapio.filter(alimento => alimento === 'cuca de uva').map(alimento => alimento.toUpperCase());*/
  
    let resultado = cardapio.map(alimento => alimento.toUpperCase());
    return resultado;
  }
  
  //console.log(cardapioIFood(true, true)); // esperado: [ 'CUCA DE UVA', 'PASTEL DE QUEIJO', 'EMPADA DE LEGUMES MARABIJOSA' ]
  
  /* Spread operator */ 
  function criarSanduiche(pao, recheio, quiejo) {
    console.log(`Seu sanduíche tem o pão ${pao} com recheio de ${recheio} e queijo ${quiejo}`);
  }
  
  const ingredientes = ['3 queijos', 'frango', 'cheddar']
  //criarSanduiche(...ingredientes);
  
  function receberValoresIndefinidos(...valores) {
    valores.map(valor => console.log(valor));
  }
  
  //receberValoresIndefinidos([1, 3, 4, 5]);
  
  /*let teste = {
    nome: "Marcos",
    idade: 29
  }
  console.log( ... teste );  //só separar a string*/
  
//   console.log( [... "Marcos"] );  //transforma string em array
//   console.log( ... "Marcos" );    //só separar a string
//   console.log( {... "Marcos"} );  //transforma string em objeto

// window ou document

let inputTeste = document.getElementById('campoTeste');
inputTeste.addEventListener('blur', function() {
    console.log("chegou aqui");
})

// // outra forma de chamar o bloco de cima
// let inputTeste = document.getElementById('campoTeste');
// inputTeste.addEventListener('blur', () => cardapioIFood(true, true));

// let objetoTeste = {
//   nome: "Kevin"
// }

// console.log(objetoTeste);

/* Prototype - forma de tipar nossas funções */

String.prototype.correr = function(upper = false){  //qualquer string pode chamar essa função
  let texto = `${this} estou correndo`;  //this = string que ta chamando a função vai concatenar com o texto
  return upper ? texto.toUpperCase() : texto;
}

console.log("eu".correr());
// console.log(1.correr());  //gera erro porque não aceita inteiro, assim forço a tipagem da função
