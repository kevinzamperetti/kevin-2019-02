import React from 'react';

const EpisodioPadrao = props => {
    const { episodio } = props
    return (
        <React.Fragment>
          <h2>{ episodio.nome }</h2>
          <img className="imagem" src={ episodio.thumbUrl } alt={ episodio.nome }/>
          <h2>Já Assisti? { episodio.assistido ? 'Sim' : 'Não' }, { episodio.qtdVezesAssistido } vez(es)</h2>
          
          <h4>Nota: { episodio.nota || 'Sem nota' }</h4>
          
          <button className="button" onClick={ () => props.sortearNoComp() }>Próximo</button>
          <button className="button" onClick={ props.marcarNoComp }>Já assisti</button>
        </React.Fragment>
    )
}

export default EpisodioPadrao;