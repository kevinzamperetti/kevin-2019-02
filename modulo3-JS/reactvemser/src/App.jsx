import React, { Component } from 'react';
// import logo from './logo.svg';
import './App.css';
import ListaEpisodios from './models/ListaEpisodios';
import EpisodioPadrao from './components/EpisodioPadrao';
import TesteRenderizacao from './components/TesteRenderizacao';

class App extends Component {
  constructor( props ) {
    super( props )
    this.listaEpisodios = new ListaEpisodios()
    // this.sortear = this.sortear.bind( this )  // Se usar assim lá embaixo na chamada só passa o atributo
    this.state = { 
      episodio: this.listaEpisodios.episodiosAleatorios,
      exibirMensagem: false
     }
    // console.log(this.ListaEpisodios);
  }

  sortear = () => {
    const episodio = this.listaEpisodios.episodiosAleatorios
    this.setState( {
      episodio
    } )
  }

  marcarComoAssistido = () => {
    const { episodio } = this.state
    this.listaEpisodios.marcarComoAssistido( episodio )
    this.setState( {
      episodio
    } )
  }

  registrarNota( evt ) {
    const { episodio } = this.state
    const notaDigitada = evt.target.value
    episodio.avaliar( notaDigitada )  // "evt.target.value" para pegar o valor do evento
    this.setState( {
      episodio,
      exibirMensagem: true
    } )
    setTimeout(() => {
      this.setState( { 
        exibirMensagem: false
       } )
    }, 5000);
  }

  // https://pt-br.reactjs.org/docs/conditional-rendering.html
  gerarCampoDeNota() {
    return (
        <div>
        {
          this.state.episodio.assistido && (
            <div>
              <h4>Avalie este episódio com uma nota:</h4>
              <input type="number" placeholder="Digite uma nota entre 1 a 5" onBlur={ this.registrarNota.bind( this ) }/>
            </div>
          )
        }
        </div>
    )
  }

  mensagemNotaInserida() {
    return (
      <div className="notaInserida">
      {
        (this.state.episodio.nota > 0) && (
          <h4>Nota registrada com sucesso!</h4>
        )
      }
      </div>
    )
  }

  render() {
    const { episodio, exibirMensagem } = this.state

    return (
      <div className="App">
        <div className="App-Header" >
          <EpisodioPadrao episodio={ episodio } sortearNoComp={ this.sortear } marcarNoComp={ this.marcarComoAssistido.bind( this ) }/>
          { this.gerarCampoDeNota() }
          <h4>Duração: { episodio.duracaoEmMin }</h4>
          <h4>Temporada/Episódio: { episodio.temporadaEpisodio }</h4>
          { exibirMensagem ? <div className="notaInserida">Nota registrada com sucesso!</div> : '' }
          {/* <TesteRenderizacao nome={ 'Kevin' }/>  */}
          {/* <TesteRenderizacao> {/* nome={ 'Kevin' } 
            <h4>Aqui novamente</h4>
          </TesteRenderizacao> */}
        </div>
      </div>
    )
  }
  
}

export default App;
