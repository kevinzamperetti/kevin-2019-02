import React, { Component } from 'react';
// import logo from './logo.svg';
import './App.css';
import ListaTime from './models/ListaEpisodios';
// import CompA, { CompB } from './ExemploComponenteBasico';
// import Filho from './exemplos/Filhos';
import Familia from './exemplos/Familia';

class App extends Component {
  constructor( props ) {
    super( props )
    this.listaTime = new ListaTime()
    console.log(this.listaTime);
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          {/* <Filho titulo = { 'Nome' } /> */}
          <Familia nome = { 'Antonio' } sobrenome = { 'Pereira' }/>
          <Familia nome = { 'Pedro' } sobrenome = { 'Pereira' }/>
          <Familia nome = { 'Maria' } sobrenome = { 'Pereira' }/>
          <Familia nome = { 'Carlos' } sobrenome = { 'Pereira' }/>

          <ul>
            { this.listaTime.todos.map( ( time ) => {
                return `Time: ${ time.nome } | Estado: ${ time.estado } `
            } ) }
          </ul>
        </header>
      </div>
    );
  }
  
}

// function App() {
  
// }

export default App;
