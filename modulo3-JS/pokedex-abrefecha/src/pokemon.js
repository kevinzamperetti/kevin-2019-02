class Pokemon { // eslint-disable-line no-unused-vars
  constructor( obj ) {
    this.id = obj.id
    this.nome = obj.name
    this.imagem = obj.sprites.front_default
    // this.imagem = "https://assets.pokemon.com/assets/cms2/img/pokedex/full/"
    this.altura = obj.height
    this.peso = obj.weight
    this.tipos = obj.types.map( listaTipos => listaTipos.type.name )
    this.estatisticas = obj.stats.map( listaEstatisticas => `${ listaEstatisticas.stat.name } (${ listaEstatisticas.base_stat }%)` )
    // console.log(this.tipos)
  }

  get getAltura() {
    return this.altura * 10
  }

  get getPeso() {
    return this.peso / 10
  }
}
