const pokeApi = new PokeApi()
const divDadosPokemon = document.querySelector( '.div-dadosPokemon' )
const inputNroPokemon = document.getElementById( 'nroPokemon' )
const buttonEtouComSorte = document.getElementById( 'estouComSorte' )
const buttonFecharPokedex = document.getElementById( 'fecharPokedex' )
let pokemonJaPesquisado = false;
const customAlert = alert

function renderizacaoPokemon( pokemon ) {
  const titleHtml = document.querySelector( '.title' )
  const title = `${ pokemon.nome.substr( 0, 1 ).toUpperCase() + pokemon.nome.substr( 1 ) } | Pokédex`

  const dados = `
        <div>
            <img src=${ pokemon.imagem }>
            <p class="pokemon">
              #${ pokemon.id }<br>
              ${ pokemon.nome.substr( 0, 1 ).toUpperCase() + pokemon.nome.substr( 1 ) }
            </p>
            <p>
              Altura: ${ pokemon.getAltura } cm<br>
              Peso: ${ pokemon.getPeso } kg<br>
              Tipos:<br>
            ${ pokemon.tipos }
            </p>
            <p>
              <ul class="multicolumn">
                ${ pokemon.estatisticas.map( lista2 => `<li>${ lista2.substr( 0, 1 ).toUpperCase() + lista2.substr( 1 ) } </li>` ).join( '' ) }
              </ul>  
            </p>
        </div>`

  divDadosPokemon.style.display = 'block'
  divDadosPokemon.innerHTML = dados
  titleHtml.innerHTML = title
}

function idPokemonNaoEncontrado() {
  inputNroPokemon.value = ''
  customAlert( 'Digite um ID válido!' )
  inputNroPokemon.focus()
}

function buscaPokemonAleatorio() {
  const nroAleatorio = Math.floor( Math.random() * 10 + 1 )

  // Não repetir pokémons aleatórios gerados
  if ( sessionStorage.getItem( nroAleatorio ) == null ) {
    sessionStorage.setItem( nroAleatorio, nroAleatorio )

    inputNroPokemon.value = nroAleatorio
    const pokemonEspecifico = pokeApi.buscarEspecifico( nroAleatorio )
    document.cookie = `id= ${ pokemonEspecifico }`
    // const x = document.cookie
    // customAlert( x )

    pokemonEspecifico
      .then( pokemon => {
        const poke = new Pokemon( pokemon )
        renderizacaoPokemon( poke )
      } )
      .catch( () => { // para tratar o erro
        idPokemonNaoEncontrado()
      } )
  } else {
    customAlert( `ID ${ nroAleatorio }. Já pesquisado!` )
  }
}

function buscaPokemonEspecifico() {
  const pokemonEspecifico = pokeApi.buscarEspecifico( inputNroPokemon.value )

  // if ( pokemonJaPesquisado === inputNroPokemon.value ) {
  //   customAlert( 'Este Pokémon já está sendo exibido!' )
  // }

  pokemonEspecifico
    .then( pokemon => {
      const poke = new Pokemon( pokemon )
      renderizacaoPokemon( poke )
    } )
    .catch( () => { // para tratar o erro
      idPokemonNaoEncontrado()
    } )
  pokemonJaPesquisado = inputNroPokemon.value
}

function fecharPokedex() {
  divDadosPokemon.style.display = 'none'
}

inputNroPokemon.addEventListener( 'blur', () => {
  if ( inputNroPokemon.value !== '' && inputNroPokemon.value > 0 ) {
    buscaPokemonEspecifico()
  }
} )

buttonEtouComSorte.addEventListener( 'click', () => {
  buscaPokemonAleatorio()
} )

buttonFecharPokedex.addEventListener( 'click', () => {
  fecharPokedex()
} )
