let pokeApi = new PokeApi();
let inputNroPokemon = document.getElementById('nroPokemon');
let buttonEtouComSorte = document.getElementById('estouComSorte');

// let nome = document.querySelector('.nome');

inputNroPokemon.addEventListener('blur', () => {
    if (inputNroPokemon.value != '' && inputNroPokemon.value > 0) {
        buscaPokemonEspecifico();
    }
})

buttonEtouComSorte.addEventListener('click', () => {
    buscaPokemonAleatorio();
})

function buscaPokemonAleatorio() {
    let nroAleatorio = Math.floor(Math.random()*807 + 1);
    inputNroPokemon.value = nroAleatorio;
    let pokemonEspecifico = pokeApi.buscarEspecifico(nroAleatorio);
    
    pokemonEspecifico
    .then( pokemon => { 
        let poke = new Pokemon ( pokemon );
        renderizacaoPokemon( poke );
    })
    .catch( () => {  //para tratar o erro
        idPokemonNaoEncontrado();
    }) 
}

function buscaPokemonEspecifico() {
    let pokemonEspecifico = pokeApi.buscarEspecifico(inputNroPokemon.value);
    
    pokemonEspecifico
    .then( pokemon => { 
        let poke = new Pokemon ( pokemon );
        renderizacaoPokemon( poke );
    })
    .catch( () => {  //para tratar o erro
        idPokemonNaoEncontrado();
    }) 
}

function renderizacaoPokemon( pokemon ) {
    let titleHtml = document.querySelector('.title');
    let title = `${pokemon.nome.substr(0,1).toUpperCase()+pokemon.nome.substr(1)} | Pokédex`;

    let divDadosPokemon = document.querySelector('.div-dadosPokemon');
    let dados =
        `<div class="row">
            <article class="col col-sm-100 col-md-100 col-lg-50" >
                <p>ID: ${pokemon.id}<br>
                    Nome: ${pokemon.nome.substr(0,1).toUpperCase()+pokemon.nome.substr(1)}<br>
                    Altura: ${pokemon.altura} cm<br>
                    Peso: ${pokemon.peso} kg<br>
                </p>
                
                <div class="row">
                    <article class="col col-sm-100 col-md-100 col-lg-50">
                        <p>
                            <ul>Tipos:
                                ${pokemon.tipos.map(lista1 => `<li>${lista1.substr(0,1).toUpperCase()+lista1.substr(1)} </li>`).join('')}
                            </ul>
                        </p>
                    </article>
                    <article class="col col-sm-100 col-md-100 col-lg-50" >
                        <p>
                            <ul>Estatísticas:
                                ${pokemon.estatisticas.map(lista2 => `<li>${lista2.substr(0,1).toUpperCase()+lista2.substr(1)} </li>`).join('')}
                            </ul> 
                        </p>
                    </article>
                </div>
            </article>

            <article class="col col-sm-100 col-md-50 col-lg-50" >
                <img class="article-image" height="400px" src=${pokemon.imagem}${("00" + pokemon.id).slice(-3)}.png>
            </article>
        </div>`

    divDadosPokemon.innerHTML = dados;
    titleHtml.innerHTML = title;
}

function idPokemonNaoEncontrado() {
    inputNroPokemon.value = '';
    alert('Digite um ID válido!')
}

