class Pokemon {
    constructor( obj ) {
        this.id = obj.id;
        this.nome = obj.name;
        // this.imagem = obj.sprites.front_default;
        this.imagem = "https://assets.pokemon.com/assets/cms2/img/pokedex/full/"
        this.altura = obj.height * 10;
        this.peso = obj.weight / 10;
        this.tipos = obj.types.map(listaTipos => listaTipos.type.name);
        this.estatisticas = obj.stats.map(listaEstatisticas => `${listaEstatisticas.stat.name} (${listaEstatisticas.base_stat}%)`);
        //  console.log(this.tipos);
    }
}