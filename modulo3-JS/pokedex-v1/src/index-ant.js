/* let pokeApi = new PokeApi();

let pokemonEspecifico = pokeApi.buscarEspecifico(12);

pokemonEspecifico
    .then( pokemon => { 
        let poke = new Pokemon ( pokemon );
        renderizacaoPokemon( poke );
})

function renderizacaoPokemon( pokemon ) {
    let dadospokemon = document.getElementById('dadosPokemon')
    // console.log(pokemon);
    let nome = document.querySelector('.nome');
    nome.innerHTML = pokemon.nome;
} */

/* let pokeApi = new PokeApi();
let inputNroPokemon = document.getElementById('nroPokemon');

inputNroPokemon.addEventListener('blur', () => {

    let pokemonEspecifico = pokeApi.buscarEspecifico(inputNroPokemon.value);
    let nome = document.querySelector('.nome');

    pokemonEspecifico
        .then( pokemon => { 
            let poke = new Pokemon ( pokemon );
            renderizacaoPokemon( poke );
        })
        .catch( () => {  //para tratar o erro
            idPokemonNaoEncontrado();
          }) 

    function renderizacaoPokemon( pokemon ) {
        nome.innerHTML = pokemon.nome;
    }

    function idPokemonNaoEncontrado() {
        nome.innerHTML = '';
            inputNroPokemon.value = '';
            alert('Digite um ID válido!')
    }

}) */

let pokeApi = new PokeApi();
let inputNroPokemon = document.getElementById('nroPokemon');
let nome = document.querySelector('.nome');

inputNroPokemon.addEventListener('blur', () => {
    buscaPokemon();
})

function buscaPokemon() {
    let pokemonEspecifico = pokeApi.buscarEspecifico(inputNroPokemon.value);
    
    pokemonEspecifico
    .then( pokemon => { 
        let poke = new Pokemon ( pokemon );
        renderizacaoPokemon( poke );
    })
    .catch( () => {  //para tratar o erro
        idPokemonNaoEncontrado();
    }) 
}

function renderizacaoPokemon( pokemon ) {
    nome.innerHTML = pokemon.nome;
}

function idPokemonNaoEncontrado() {
    nome.innerHTML = '';
        inputNroPokemon.value = '';
        alert('Digite um ID válido!')
}