/* valida-formulario */

// Criar um formulário de contato no site de vocês (Nome, email, telefone, assunto), 
// ao sair do input de nome tem que garantir que tenha no minimo 10 caracteres, 
// garantir que em email tenha @ 
// e ao clicar no botão de enviar tem que garantir que não tenha campos em branco.

// function validaNome() {
//     if (inputNome.value.length < 10) {
//         alert('Mínimo de caracteres para o nome é 10.');
//     }
// }

// function validaEmail() {
//     if (!inputEmail.value.includes('@')) {
//         alert('E-mail no formato inválido. Ex: seu@email.com');
//     }
// }

let inputNome = document.getElementById("nome");   
let inputEmail = document.getElementById("email");   
let inputTelefone = document.getElementById("telefone");   
let inputAssunto = document.getElementById("assunto");   
let inputMensagem = document.getElementById("mensagem");  

inputNome.addEventListener('blur', () => {
    if (inputNome.value.length < 10) {
        alert('Mínimo de caracteres inválido. Campo nome necessita de 10 caracteres.');
    }
})

inputEmail.addEventListener('blur', () => {
    if (!inputEmail.value.includes('@')) {
        alert('E-mail no formato inválido. Ex: seu@email.com');
    }
})

function validaCampos() {
    if ( inputNome.value == '' || inputEmail.value == '' || inputTelefone.value == '' ||
         inputAssunto.value == '' || inputMensagem.value == '') {
        alert('Todos os campos devem estar preenchidos');
    }
}