function multiplicar(multiplicador, ...valores) {
    return valores.map(valor => valor * multiplicador);
}

console.log(multiplicar(5, 3, 4)); // [15, 20]								                                                                        
console.log(multiplicar(5, 3, 4, 5)); // [15, 20, 25]

