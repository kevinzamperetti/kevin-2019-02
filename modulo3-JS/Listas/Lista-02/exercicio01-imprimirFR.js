function arredondar(numero, precisao = 2) {
    const fator = Math.pow(10, precisao);
    return Math.ceil( numero * fator) / fator;
}

function imprimirFR(numero) {
    let qtdCasasMilhares = 3;
    let separadorMilhar = ".";
    let separadorDecimal = ",";

    let stringBuffer = [];
    let parteDecimal = arredondar(Math.abs(numero)%1);  //pega numero absoluto
    let parteInteira = Math.trunc(numero); //pegar somente parte inteira, eliminando os decimais
    let parteInteiraString = Math.abs(parteInteira).toString();
    let parteInteiraTamanho = parteInteiraString.length;

    let c = 1;
    while (parteInteiraString.length > 0) {
        if (c % qtdCasasMilhares == 0) {
            stringBuffer.push(`${separadorMilhar}${parteInteiraString.slice(parteInteiraTamanho - c)}`);  //push adiciona no array
            parteInteiraString = parteInteiraString.slice(0, parteInteiraTamanho - c);  //slice corta um pedaço da string da posição 0 menos o que tirei do milhar
        } else if (parteInteiraString.length < qtdCasasMilhares) {
            stringBuffer.push(parteInteiraString);
            parteInteiraString = "";
        }
        c++;
    }
    stringBuffer.push(parteInteiraString);
    
    let decimalString = parteDecimal.toString().replace('0.', '').padStart(2, '0');
    return `${parteInteira >= 0 ? '' : '-'}${stringBuffer.reverse().join('')}${separadorDecimal}${decimalString} €`;
}

// console.log(imprimirFR(0)); // “0,00 €”
// console.log(imprimirFR(3498.99)); // “3.498,99 €”
// console.log(imprimirFR(-3498.99)); // “-3.498,99 €”
// console.log(imprimirFR(2313477.0135)); // “2.313.477,02 €”


function imprimirFR2(numero) {
    //fixa o numero com 2 casas decimais e separa a string em um array de 2 partes (antes e depois do ponto).
    numero = numero.toFixed(2).split('.');   
    
    //exclui as casas decimais (numero[0])
    numero[0] = numero[0].split(/(?=(?:...)*$)/).join('.');  
    
    //retorna o número formatado juntando com as casas decimais usando a virgula
    return `${numero.join(',')} €`;  
}

// console.log(imprimirFR2(0)); // “0,00 €”
// console.log(imprimirFR2(3498.99)); // “3.498,99 €”
// console.log(imprimirFR2(-3498.99)); // “-3.498,99 €”
// console.log(imprimirFR2(2313477.0135)); // “2.313.477,02 €”
