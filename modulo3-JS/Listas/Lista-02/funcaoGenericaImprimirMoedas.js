

/* Função genérica de conversão de moedas */ 

let moedas = ( function () {
    
    //Tudo que é privado aqui
    function imprimirMoeda( params) {

        function arredondar(numero, precisao = 2) {
            const fator = Math.pow(10, precisao);
            return Math.ceil( numero * fator) / fator;
        }

        const {
            numero,
            separadorMilhar,
            separadorDecimal,
            colocarMoeda,
            colocarNegativo
        } = params;
    
        let qtdCasasMilhares = 3;
        let stringBuffer = [];
        let parteDecimal = arredondar(Math.abs(numero)%1);  //pega numero absoluto
        let parteInteira = Math.trunc(numero); //pegar somente parte inteira
        let parteInteiraString = Math.abs(parteInteira).toString();
        let parteInteiraTamanho = parteInteiraString.length;
    
        let c = 1;
        while (parteInteiraString.length > 0) {
            if (c % qtdCasasMilhares == 0) {
                stringBuffer.push(`${separadorMilhar}${parteInteiraString.slice(parteInteiraTamanho - c)}`);  //push adiciona no array
                parteInteiraString = parteInteiraString.slice(0, parteInteiraTamanho - c);
            } else if (parteInteiraString.length < qtdCasasMilhares) {
                stringBuffer.push(parteInteiraString);
                parteInteiraString = "";
            }
            c++;
        }
        stringBuffer.push(parteInteiraString);
        
        let decimalString = parteDecimal.toString().replace('0.', '').padStart(2, '0');
        const numeroFormatado = `${ stringBuffer.reverse().join('') }${ separadorDecimal }${ decimalString }`;
       
        return parteInteira >= 0 ? colocarMoeda(numeroFormatado) : colocarNegativo(colocarMoeda(numeroFormatado));
    }

    //Tudo que é público
    return {
        imprimirBRL: (numero) => 
            imprimirMoeda({
                numero,
                separadorMilhar: '.',
                separadorDecimal: ',',
                colocarMoeda: numeroFormatado => `R$ ${ numeroFormatado }`,
                colocarNegativo: numeroFormatado => `-${ numeroFormatado }`
            }),
        
        imprimirGBP: (numero) => 
            imprimirMoeda({
                numero,
                separadorMilhar: ',',
                separadorDecimal: '.',
                colocarMoeda: numeroFormatado => `£ ${ numeroFormatado }`,
                colocarNegativo: numeroFormatado => `-${ numeroFormatado }`
            }),

         imprimirFR: (numero) => 
            imprimirMoeda({
                numero,
                separadorMilhar: '.',
                separadorDecimal: ',',
                colocarMoeda: numeroFormatado => `${ numeroFormatado } €`,
                colocarNegativo: numeroFormatado => `-${ numeroFormatado }`
            })
    }
})()

console.log(moedas.imprimirBRL(0)); // “R$ 0,00”
console.log(moedas.imprimirBRL(3498.99)); // “R$ 3.498,99”
console.log(moedas.imprimirBRL(-3498.99)); // “-R$ 3.498,99”
console.log(moedas.imprimirBRL(2313477.0135)); // “R$ 2.313.477,02”

console.log(moedas.imprimirGBP(0)); // “R$ 0,00”
console.log(moedas.imprimirGBP(3498.99)); // “R$ 3.498,99”
console.log(moedas.imprimirGBP(-3498.99)); // “-R$ 3.498,99”
console.log(moedas.imprimirGBP(2313477.0135)); // “R$ 2.313.477,02”

console.log(moedas.imprimirFR(0)); // “R$ 0,00”
console.log(moedas.imprimirFR(3498.99)); // “R$ 3.498,99”
console.log(moedas.imprimirFR(-3498.99)); // “-R$ 3.498,99”
console.log(moedas.imprimirFR(2313477.0135)); // “R$ 2.313.477,02”