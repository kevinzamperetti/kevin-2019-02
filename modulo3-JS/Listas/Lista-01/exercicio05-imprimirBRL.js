// var formatter = new Intl.NumberFormat('pt-BR', {
//     style: 'currency',
//     currency: 'BRL',
//     minimumFractionDigits: 2,
//   });
//
// function imprimirBRL(numero) {
//     let valor = parseFloat(numero);
//     let valor_formatado = formatter.format(valor);
//     return `${ valor_formatado}`;
// }

function imprimirBRL(numero) {
    //fixa o numero com 2 casas decimais e separa a string em um array de 2 partes (antes e depois do ponto).
    numero = numero.toFixed(2).split('.');   
    
    //exclui as casas decimais (numero[0])
    numero[0] = "R$ " + numero[0].split(/(?=(?:...)*$)/).join('.');  
    
    //retorna o número formatado juntando com as casas decimais usando a virgula
    return `${numero.join(',')}`;  
}

console.log(imprimirBRL(0)); // “R$ 0,00”
console.log(imprimirBRL(3498.99)); // “R$ 3.498,99”
console.log(imprimirBRL(-3498.99)); // “-R$ 3.498,99”
console.log(imprimirBRL(2313477.0135)); // “R$ 2.313.477,02”
