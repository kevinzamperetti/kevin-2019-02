function somarPares(numeros){
    let soma = 0;
    for (let i = 0; i < numeros.length; i++) {
        if (i % 2 === 0) {
            soma += numeros[i];
        }
    } 
    return soma;
}

console.log( somarPares([ 1, 56, 4.34, 6, -2 ]) ); // 3.34

