const circulo = {
    raio: 3,
    tipoCalculo: "C"
}

function calcularCirculo({raio, tipoCalculo}){
    if(tipoCalculo == "A"){
        return Math.PI * Math.pow(raio, 2);
    }else{
        return 2 * Math.PI * raio;
    }

}

console.log(calcularCirculo(circulo));