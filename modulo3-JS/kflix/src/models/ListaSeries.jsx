import Series from "./Series";
import PropTypes from 'prop-types';

function _sortear( min, max ) {
  min = Math.ceil( min )
  max = Math.floor( max )
  return Math.floor( Math.random() * ( max - min ) ) + min
}

export default class ListaSeries {
    constructor() {
        this.todas = [
             {titulo:"Stranger Things", anoEstreia:2016, diretor:["Matt Duffer","Ross Duffer"], genero:["Suspense","Ficcao Cientifica","Drama"], elenco:["Winona Ryder","David Harbour","Finn Wolfhard","Millie Bobby Brown","Gaten Matarazzo","Caleb McLaughlin","Natalia Dyer","Charlie Heaton","Cara Buono","Matthew Modine","Noah Schnapp"], temporadas:2, numeroEpisodios:17, distribuidora:"Netflix", imagem:"https://upload.wikimedia.org/wikipedia/commons/3/38/Stranger_Things_logo.png"},
             {titulo:"Game Of Thrones", anoEstreia:2011, diretor:["David Benioff","D. B. Weiss","Carolyn Strauss","Frank Doelger","Bernadette Caulfield","George R. R. Martin"], genero:["Fantasia","Drama"], elenco:["Peter Dinklage","Nikolaj Coster-Waldau","Lena Headey","Emilia Clarke","Kit Harington","Aidan Gillen","Iain Glen ","Sophie Turner","Maisie Williams","Alfie Allen","Isaac Hempstead Wright"], temporadas:7, numeroEpisodios:67, distribuidora:"HBO", imagem:"https://desejosdebeleza.com/wp-content/uploads/2014/04/game-of-thones-capa.jpg"},
             {titulo:"The Walking Dead", anoEstreia:2010, diretor:["Jolly Dale","Caleb Womble","Paul Gadd","Heather Bellson"], genero:["Terror","Suspense","Apocalipse Zumbi"], elenco:["Andrew Lincoln","Jon Bernthal","Sarah Wayne Callies","Laurie Holden","Jeffrey DeMunn","Steven Yeun","Chandler Riggs ","Norman Reedus","Lauren Cohan","Danai Gurira","Michael Rooker ","David Morrissey"], temporadas:9, numeroEpisodios:122, distribuidora:"AMC", imagem:"https://www.thewalkingdead.com.br/wp-content/uploads/2013/10/the-walking-dead-logo.jpg"},
             {titulo:"Band of Brothers", anoEstreia:20001, diretor:["Steven Spielberg","Tom Hanks","Preston Smith","Erik Jendresen","Stephen E. Ambrose"], genero:["Guerra"], elenco:["Damian Lewis","Donnie Wahlberg","Ron Livingston","Matthew Settle","Neal McDonough"], temporadas:1, numeroEpisodios:10, distribuidora:"HBO", imagem:"https://http2.mlstatic.com/band-of-brothers-minisserie-completa-D_NQ_NP_942137-MLB26969576142_032018-F.jpg"},
             {titulo:"The JS Mirror", anoEstreia:2017, diretor:["Lisandro","Jaime","Edgar"], genero:["Terror","Caos","JavaScript"], elenco:["Daniela Amaral da Rosa","Antônio Affonso Vidal Pereira da Rosa","Gustavo Lodi Vidaletti","Bruno Artêmio Johann Dos Santos","Márlon Silva da Silva","Izabella Balconi de Moura","Diovane Mendes Mattos","Luciano Maciel Figueiró","Igor Ceriotti Zilio","Alexandra Peres","Vitor Emanuel da Silva Rodrigues","Raphael Luiz Lacerda","Guilherme Flores Borges","Ronaldo José Guastalli","Vinícius Marques Pulgatti"], temporadas:1, numeroEpisodios:40, distribuidora:"DBC", imagem:"https://i0.wp.com/oportaln10.com.br/wp-content/uploads/2019/01/javascript.jpg?resize=710%2C400&ssl=1"},
             {titulo:"10 Days Why", anoEstreia:2010, diretor:["Brendan Eich"], genero:["Caos","JavaScript"], elenco:["Brendan Eich","Bernardo Bosak"], temporadas:10, numeroEpisodios:10, distribuidora:"JS", imagem:"https://i.ytimg.com/vi/HQuJzGU7TWg/maxresdefault.jpg"},
             {titulo:"Mr. Robot", anoEstreia:2018, diretor:["Sam Esmail"], genero:["Drama","Techno Thriller","Psychological Thriller"], elenco:["Rami Malek","Carly Chaikin","Portia Doubleday","Martin Wallström","Christian Slater"], temporadas:3, numeroEpisodios:32, distribuidora:"USA Network", imagem:"https://4gnews.pt/wp-content/uploads/2017/12/mr.-robot-1.jpg"},
             {titulo:"Narcos", anoEstreia:2015, diretor:["Paul Eckstein","Mariano Carranco","Tim King","Lorenzo O Brien"], genero:["Documentario","Crime","Drama"], elenco:["Wagner Moura","Boyd Holbrook","Pedro Pascal","Joann Christie","Mauricie Compte","André Mattos","Roberto Urbina","Diego Cataño","Jorge A. Jiménez","Paulina Gaitán","Paulina Garcia"], temporadas:3, numeroEpisodios:30, distribuidora:null, imagem:"http://www.medellincitytours.com/wp-content/uploads/2018/07/419.jpg"},
             {titulo:"Westworld", anoEstreia:2016, diretor:["Athena Wickham"], genero:["Ficcao Cientifica","Drama","Thriller","Acao","Aventura","Faroeste"], elenco:["Anthony I. Hopkins","Thandie N. Newton","Jeffrey S. Wright","James T. Marsden","Ben I. Barnes","Ingrid N. Bolso Berdal","Clifton T. Collins Jr.","Luke O. Hemsworth"], temporadas:2, numeroEpisodios:20, distribuidora:"HBO", imagem:"https://cdnstatic8.com/cinemaginando.com/wp-content/uploads/Westworld-Logo.png"},
             {titulo:"Breaking Bad", anoEstreia:2008, diretor:["Vince Gilligan","Michelle MacLaren","Adam Bernstein","Colin Bucksey","Michael Slovis","Peter Gould"], genero:["Acao","Suspense","Drama","Crime","Humor Negro"], elenco:["Bryan Cranston","Anna Gunn","Aaron Paul","Dean Norris","Betsy Brandt","RJ Mitte"], temporadas:5, numeroEpisodios:62, distribuidora:"AMC", imagem:"https://feriasdecinema.com/wp-content/uploads/2017/04/breaking-bad-capa.jpg"}
        ].map( e => new Series( e.titulo, e.anoEstreia, e.diretor, e.genero, e.elenco, e.temporadas, e.numeroEpisodios, e.distribuidora, e.imagem ) )
    }

    get episodiosAleatorios() {
      const indice = _sortear( 0, this.todas.length )
      return this.todas[ indice ]
    }

    possuiCamposNull(indice) {
      if ( this.todas[indice].titulo === null  ||
            this.todas[indice].anoEstreia === null  ||
            this.todas[indice].diretor === null  ||
            this.todas[indice].genero === null  ||
            this.todas[indice].elenco === null  ||
            this.todas[indice].temporadas === null  ||
            this.todas[indice].numeroEpisodios === null  ||
            this.todas[indice].distribuidora === null ) {
        return true
      }
      return false
    }

    invalidas() {
      let dataAtual = new Date().getFullYear();
      let seriesInvalidas = []
      for (let i = 0; i < this.todas.length; i++) {
        if ( this.todas[i].anoEstreia > dataAtual || this.possuiCamposNull(i) === true ) {
          seriesInvalidas.push(this.todas[i])
        }
      }
      return seriesInvalidas;
    }

    filtrarPorAno( ano ) {
      let seriesDoAno = []
      for (let i = 0; i < this.todas.length; i++) {
        if ( this.todas[i].anoEstreia >= ano ) {
          seriesDoAno.push(this.todas[i])
        }
      }
      return seriesDoAno;
    }
    
    procurarPorNome( nome ) {
      if (this.todas.find( e => e.elenco.includes( nome )  )) {
        return true
      } else {
        return false
      }
    }
    
    mediaDeEpisodios(  ) {
      let somaEpisodios = 0
      let totalEpisodios = 0
      for (let i = 0; i < this.todas.length; i++) {
        somaEpisodios += this.todas[i].numeroEpisodios
        totalEpisodios += 1 
      }
      return somaEpisodios / totalEpisodios;
    }

    totalSalarios( indice = 0 ) {
      indice = indice !== '' && indice <= 9 ? indice : 0
      
      const titulo = this.todas[indice].titulo
      const imagem = this.todas[indice].imagem

      const salarioDiretor = 100000
      const salarioOperario = 40000;
      const totalDeDiretores = this.todas[indice].diretor.length
      const totalDeOperarios = this.todas[indice].elenco.length
      const totalSalarioDiretores = totalDeDiretores * salarioDiretor
      const totalSalarioOperarios = totalDeOperarios * salarioOperario
      const salarioTotal = (totalSalarioDiretores + totalSalarioOperarios).toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' })
      
      return { titulo, imagem, salarioTotal };
    }

    queroGenero( genero ) {
      let seriesDoGenero = this.todas.map( e => e.genero.includes( genero ) ? e : null).filter(e => e != null)
      return seriesDoGenero
    }

    queroTitulo( titulo ) {
      let seriePesquisada = this.todas.map( e => e.titulo.includes( titulo ) ? e : null).filter(e => e != null)
      return seriePesquisada
    }

    creditos( indice) {
      indice = indice !== '' && indice <= 9 ? indice : 0

      const imagem = this.todas[indice].imagem
      const titulo = this.todas[indice].titulo
      let diretores = this.todas[indice].diretor.map( diretor => diretor.split(' ').reverse().join(' ') )
      diretores.sort()
      diretores = diretores.map(diretor => diretor.split(' ').reverse().join(' ') )

      let elenco = this.todas[indice].elenco.map( elenco => elenco.split(' ').reverse().join(' ') )
      elenco.sort()
      elenco = elenco.map(elenco => elenco.split(' ').reverse().join(' ') )

      return { imagem, titulo, diretores, elenco }
      // return `Título: ${titulo} - Diretores: ${diretores} - Elenco: ${elenco}`
    }

    matchesNomeAbreviado(s) {
      let verifica = s.match( '\\s+[a-zA-Z]{1}\.\\s+' )
      return ( verifica )? Boolean(verifica[0]) : undefined
    }
      
    letraAbreviada(s) {
      if(this.matchesNomeAbreviado(s)) return s.match( '\\s+[a-zA-Z]{1}\.\\s+' )[0].charAt(1)
    }
      
    serieComTodosNomesAbreviados() {
      return this.todas.filter( (serie) => {
        return !serie.elenco.find( s => !this.matchesNomeAbreviado(s) )
      } )
    }
      
    mostrarPalavraSecreta() {
      let palavra = "#"
      let array = this.serieComTodosNomesAbreviados()
      array[0].elenco.forEach( (s) => {
        palavra = palavra.concat(this.letraAbreviada(s))
      })
      return palavra
    }    
}

ListaSeries.propTypes = {
  invalidas: PropTypes.array,
  filtrarPorAno: PropTypes.array,
  procurarPorNome: PropTypes.array,
  mediaDeEpisodios: PropTypes.array,
  totalSalarios: PropTypes.array,
  queroGenero: PropTypes.array,
  queroTitulo: PropTypes.array,
  creditos: PropTypes.array,

}


