import PropTypes from 'prop-types';

export default class Series {
  constructor( titulo, anoEstreia, diretor, genero, elenco, temporadas, numeroEpisodios, distribuidora, imagem ) {
      this.titulo = titulo
      this.anoEstreia = anoEstreia
      this.diretor = diretor
      this.genero = genero
      this.elenco = elenco
      this.temporadas = temporadas
      this.numeroEpisodios = numeroEpisodios
      this.distribuidora = distribuidora
      this.imagem = imagem
  }

  get duracaoEmMin() {
      return `${ this.duracao } min`
  }

  get totalTemporadas() {
    return `${ this.temporadas.toString().padStart(2, '0') }`
  }

  get totalEpisodios() {
    return `${ this.numeroEpisodios.toString().padStart(2 ,'0') }`
  }
}

Series.propTypes = {
  titulo: PropTypes.string.isRequired,
  anoEstreia: PropTypes.number.isRequired,
  diretor: PropTypes.arrayOf(PropTypes.string).isRequired,
  genero: PropTypes.arrayOf(PropTypes.string).isRequired,
  elenco: PropTypes.arrayOf(PropTypes.string).isRequired,
  temporadas: PropTypes.number.isRequired,
  numeroEpisodios: PropTypes.number.isRequired,
  distribuidora: PropTypes.string.isRequired
}

