import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom'
import './css/App.css';

import Home from './pages/Home'
import Login from './pages/Login'
import InfoKflix from './pages/InfoKFlix'
import ReactVemSer from './pages/ReactVemSer'

import { PrivateRoute } from './components/PrivateRoute'

class App extends Component {
  constructor( props ) {
    super( props )
  }

  render() {
    return (
      <div className="App">
        <Router>
          <React.Fragment>
            <section className="">
              <PrivateRoute path="/" exact component={ Home } />
              <Route path="/login" component={ Login } />
              <PrivateRoute path="/reactVemSer" component={ ReactVemSer } />
              <PrivateRoute path="/infoKflix" component={ InfoKflix } />
            </section>
          </React.Fragment>
        </Router>
      </div>
    )
  }
}

export default App;
