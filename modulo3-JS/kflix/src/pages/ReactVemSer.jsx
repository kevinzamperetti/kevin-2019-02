import React, { Component } from 'react';
import * as axios from 'axios';
import '../css/Reactvemser.css';
import ListaEpisodios from '../models/ListaEpisodios';
import EpisodioPadrao from '../components/EpisodioPadrao';
import Cabecalho from '../components/Cabecalho';
import MensagemFlash from '../components/MensagemFlash';
import MeuInputNumero from '../components/MeuInputNumero';
import ListaDeAvaliacoes from '../components/ListaDeAvaliacoes'

class App extends Component {
  constructor( props ) {
    super( props )
    this.listaEpisodios = new ListaEpisodios()
    this.state = { 
      episodio: this.listaEpisodios.episodiosAleatorios,
      exibirMensagem: false,
      mensagemTipo: "verde"
    }
    // console.log(this.listaEpisodios.exibirSeriesAssistidas());
    // console.log(seriesAssistidas)
  }

  // componentDidMount() {
  //   axios.get('https://pokeapi.co/api/v2/pokemon/').then( response => console.log( response ) )
  // }

  sortear = () => {
    const episodio = this.listaEpisodios.episodiosAleatorios
    this.setState( {
      episodio
    } )
  }

  marcarComoAssistido = () => {
    const { episodio } = this.state
    this.listaEpisodios.marcarComoAssistido( episodio )
    this.setState( {
      episodio,
    } )
    console.log(this.listaEpisodios.exibirSeriesAssistidas());
    //ver se deixa o assistido como atributo da Serie e do ListaSeries
  }

  validarNota( nota ) {
    return nota >= 1 && nota <= 5
  }

  alterarEstadoExibirMensagem() {
    this.setState( {
      exibirMensagem: !this.state.exibirMensagem
    } )
  }

  registrarNota( evt ) {
    const { episodio } = this.state
    const notaDigitada = evt.target.value
    
    if ( this.validarNota(notaDigitada) ) {
      episodio.avaliar( notaDigitada )  
      this.setState( {
        mensagemTipo: "verde"
      } )
    } else {
      this.setState( {
        mensagemTipo: "vermelho"
      } )
    }

    this.setState( {
      episodio,
      exibirMensagem: true,
    } )
  }

  mensagemNotaInserida() {
    return (
      <div className="notaInserida">
      {
        (this.state.episodio.nota > 0) && (
          <h4>Nota registrada com sucesso!</h4>
        )
      }
      </div>
    )
  }

  // logout() {
  //   localStorage.removeItem('Authorization');
  // }

  render() {
    const { episodio, exibirMensagem, mensagemTipo } = this.state

    return (
      <div className="App">
        <div className="App-Header">
          <Cabecalho/>
          {/* <button type="button" onClick={ this.logout.bind( this ) } >logout</button> */}
          <EpisodioPadrao episodio={ episodio } sortearNoComp={ this.sortear } marcarNoComp={ this.marcarComoAssistido.bind( this ) }/>

          <MeuInputNumero episodio={ episodio }
                          placeholder={ "Nota deve ser entre 1 a 5" }
                          deveSerExibido={ true } 
                          texto={ "Avalie este episódio com uma nota:" } 
                          obrigatorio={ true }
                          registrarNota={ this.registrarNota.bind( this ) } />

          <MensagemFlash exibirMensagem={ exibirMensagem } mensagemTipo={ mensagemTipo } 
                         alterarEstadoExibirMensagem={ this.alterarEstadoExibirMensagem.bind( this ) }/>

          {/* <ListaDeAvaliacoes seriesAssistidas={ this.seriesAssistidas }/> */}
        </div>
      </div>
    );
  }
}

export default App;
