import React, { Component } from 'react';
import Logo from '../img/kflix-logo.png';
import '../css/App.css';
import '../css/InfoKFlix.css';
import '../css/Cabecalho.css';
import ListaSeries from '../models/ListaSeries';
import Cabecalho from '../components/Cabecalho';
import ResultadosPesquisa from '../components/ResultadosPesquisa';
import InputFuncoes from '../components/InputFuncoes';

class InfoKflix extends Component {
  constructor( props ) {
    super( props )
    this.listaSeries = new ListaSeries()
    this.setInput = this.setInput.bind( this )

    this.state = { 
      serie: this.listaSeries,
      exibir: '',
      input: ''
     }
    // console.log(this.listaSeries.creditos(0));
  }

  listarInvalidas = () => {
    this.setState( {
      exibir: 'exibirInvalidas'
    } )
  }

  listarPorAno = () => {
    this.setState( {
      exibir: 'exibirPorAno'
    } )
  }

  listarSeEhAtor = () => {
    this.setState( {
      exibir: 'exibirSeEhAtor'
    } )
  }

  listarMediaEpisodios = () => {
    this.setState( {
      exibir: 'exibirMediaDeEpisodios'
    } )
  }

  listarSalarioSerie = () => {
    this.setState( {
      exibir: 'exibirSalarioDaSerie'
    } )
  }

  listarPorGenero = () => {
    this.setState( {
      exibir: 'exibirPorGenero'
    } )
  }

  listarPorTitulo = () => {
    this.setState( {
      exibir: 'exibirPorTitulo'
    } )
  }

  listarCreditos = () => {
    this.setState( {
      exibir: 'exibirCreditos'
    } )
  }

  listarHashTag = () => {
    this.setState( {
      exibir: 'exibirHashTag'
    } )
  }

  setInput(e) {
    if(e.target.value === null) {
      e.target.value = 0
    }
    this.setState( {
      input: e.target.value
    } )
  }

  render() {
    const { serie, exibir, input } = this.state
    
    return (
      <div className="App">
        <Cabecalho/>

        <div className="header">
          <img className="logo" src={Logo} alt=""/>
          Digite o que deseja pesquisar e clique em um dos botões abaixo
          <input className="input" type="text" id="inputPesquisar" onBlur={ this.setInput } />
        </div>

        <div className="div-button">
          <input className="button-kflix" type="button" value="Séries Inválidas" onClick={ this.listarInvalidas }/>
          <input className="button-kflix" type="button" value="Séries por Ano" onClick={ this.listarPorAno }/>
          <input className="button-kflix" type="button" value="Procurar nome no Elenco" onClick={ this.listarSeEhAtor }/>
          <input className="button-kflix" type="button" value="Média de Episódios" onClick={ this.listarMediaEpisodios }/>
          <input className="button-kflix" type="button" value="Total de Salários por Série" onClick={ this.listarSalarioSerie }/>
          <input className="button-kflix" type="button" value="Séries de um Gênero específico" onClick={ this.listarPorGenero }/>
          <input className="button-kflix" type="button" value="Séries por Título específico" onClick={ this.listarPorTitulo }/>
          <input className="button-kflix" type="button" value="Créditos" onClick={ this.listarCreditos }/>
          <input className="button-kflix" type="button" value="Hashtag" onClick={ this.listarHashTag }/>
        </div>
        
        <InputFuncoes/>
        
        <ResultadosPesquisa serie={ serie } exibir={ exibir } input={ input }/>
      </div>
    )
  }
}

export default InfoKflix;
