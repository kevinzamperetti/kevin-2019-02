import React from 'react';
import { Link } from 'react-router-dom'
import '../css/Cabecalho.css';
// import '../css/App.css';

const Cabecalho = props => {

  function  logout() {
    localStorage.removeItem('Authorization');
  }

  return (
    <React.Fragment>
      <div>
        <Link className="button-cabecalho menu" to="/">Home</Link>
        <Link className="button-cabecalho menu" to="/login">Login</Link>
        <Link className="button-cabecalho menu" to="/reactVemSer">React Vem Ser</Link>
        <Link className="button-cabecalho menu" to="/infoKflix">KFlix</Link>
        <button className="button-cabecalho menu" type="button" onClick={ logout.bind( this ) } >Logout</button>
      </div>
    </React.Fragment>
  )
}

export default Cabecalho;