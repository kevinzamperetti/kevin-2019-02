import React from 'react'
import '../css/MensagemFlash.css';

const MensagemFlash = props => {
	const { exibirMensagem, mensagemTipo } = props
	const mensagemValida = <span className="fading" id="verde">Nota registrada com sucesso!</span>
	const mensagemInvalida = <span className="fading" id="vermelho">Informar uma nota válida (entre 1 e 5)</span>

	function verificaTipoMensagem() {
		return mensagemTipo === "verde" ? mensagemValida : mensagemInvalida
	}

	function definirTempoMensagem( tempo = 3000 ) {
		setTimeout(() => {
			props.alterarEstadoExibirMensagem()
			}, tempo);
	}

	return (
		<React.Fragment>
			{ exibirMensagem ? verificaTipoMensagem() : '' }
			{ exibirMensagem ? definirTempoMensagem() : '' }
		</React.Fragment>
	)
}

export default MensagemFlash;