import React from 'react';
import '../css/App.css';
import '../css/InfoKFlix.css';
import '../css/Cabecalho.css';

const ResultadosPesquisa = props => {
    const { serie, exibir, input } = props
    
    return (
        <React.Fragment>
        { exibir === 'exibirInvalidas' ? 
          <div className="dados">
            <strong>Séries inválidas: </strong>
            { 
              serie.invalidas().map( ( serie ) => (
                <article>
                  <img src={serie.imagem} alt=""/>
                  <strong> { serie.titulo } </strong>
                </article> ) 
              )
            }
          </div> : ''
        }

        { exibir === 'exibirPorAno' ? 
          <div className="dados">
            <strong className="font-white">Séries com ano de estreia em: {input}</strong>
            { 
              serie.filtrarPorAno(input).map( ( serie ) => (
                <article>
                  <img src={serie.imagem} alt=""/>
                  <strong> { serie.titulo }</strong>
                </article> )
              )
            }
          </div> : '' 
        }

        { exibir === 'exibirSeEhAtor' ? 
          <div className="dados">
              <article>
                <strong>Sou um ator? </strong>
                { serie.procurarPorNome(input) ? 'Sim' : 'Não' }
              </article>
          </div> : '' 
        }

        { exibir === 'exibirMediaDeEpisodios' ? 
          <div className="dados">
              <article>
                <strong>Média de Episódios: </strong>
                {serie.mediaDeEpisodios()}
              </article>
          </div> : '' 
        }

        { exibir === 'exibirSalarioDaSerie' ? 
          <div className="dados">
            <article>
              <img src={serie.totalSalarios(input).imagem} alt=""/>
              <strong>Salário total da Série:</strong> { serie.totalSalarios(input).salarioTotal }
            </article>
          </div> : '' 
        }

        { exibir === 'exibirPorGenero' ? 
          <div className="dados">
            <strong className="font-white">Séries que pertencem ao gênero: {input}</strong>
            { 
              serie.queroGenero(input).map( ( serie ) => (
                <article>
                  <img src={serie.imagem} alt=""/>
                  <strong> { serie.titulo }</strong>
                </article> )
              )
            }
          </div> : '' 
        }

        { exibir === 'exibirPorTitulo' ? 
          <div className="dados">
            <strong className="font-white">Séries que possuem em seu título: {input}</strong>
            { 
              serie.queroTitulo(input).map( ( serie ) => (
                <article>
                  <img src={serie.imagem} alt=""/>
                  <strong> { serie.titulo }</strong>
                </article> )
              )
            }
          </div> : '' 
        }

        { exibir === 'exibirCreditos' ? 
          <div className="dados">
            <strong className="font-white">Créditos da série de índice: {input}</strong>
            <article>
              <img src={serie.creditos(input).imagem} alt=""/>
              <strong>{ serie.creditos(input).titulo }</strong>
              <p>
                <strong>Diretores:</strong>
                { 
                  serie.creditos(input).diretores.map( ( serie ) => (
                    <span> { serie } </span>)
                  )
                }
              </p>
              <p>
                <strong>Elenco:</strong>
                { 
                  serie.creditos(input).elenco.map( ( serie ) => (
                    <span> { serie } </span> )
                  )
                }
              </p>              
            </article>
          </div> : '' 
        }

        { exibir === 'exibirHashTag' ? 
          <div className="dados">
            <article>
              <strong>HashTag:</strong> { serie.mostrarPalavraSecreta() }
            </article>
          </div> : '' 
        }       
        </React.Fragment>
    )
}

export default ResultadosPesquisa;