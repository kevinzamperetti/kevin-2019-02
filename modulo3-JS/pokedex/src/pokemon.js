class Pokemon { // eslint-disable-line no-unused-vars
  constructor( obj ) {
    this.id = obj.id
    this.nome = obj.name
    this.imagem = obj.sprites.front_default
    this.altura = obj.height
    this.peso = obj.weight
    this.tipos = obj.types.map( listaTipos => listaTipos.type.name )
    this.estatisticas = obj.stats.map( listaEstatisticas => `${ listaEstatisticas.stat.name } (${ listaEstatisticas.base_stat }%)` )
  }

  converterAltura( multiplicador = 10 ) {
    return this.altura * multiplicador;
  }

  converterPeso( divisor = 10 ) {
    return this.peso / divisor;
  }
}
