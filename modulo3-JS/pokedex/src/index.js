const pokeApi = new PokeApi()
const inputNroPokemon = document.getElementById( 'nroPokemon' )
const buttonEtouComSorte = document.getElementById( 'estouComSorte' )
let pokemonJaPesquisado = 0
const customAlert = alert



function converterPrimeiraMaiuscula( palavra ) {
  return `${ palavra.charAt( 0 ).toUpperCase() }${ palavra.substr( 1 ) }`
}

function renderizacaoPokemon( pokemon ) {

  const titleHtml = document.querySelector( '.title' )
  const title = `${ converterPrimeiraMaiuscula( pokemon.nome ) } | Pokédex`

  const divDadosPokemon = document.querySelector( '.div-dadosPokemon' )
  const dados = `
          <img class="imagem" src=${ pokemon.imagem }>
          <p class="pokemon">
            #${ pokemon.id }<br>
            ${ pokemon.nome }
          </p><br>
          <p class="texto-esquerda">
            Altura: ${ pokemon.converterAltura() } cm<br>
            Peso: ${ pokemon.converterPeso() } kg<br>
            Tipos:<br>
            ${ pokemon.tipos }
          </p>
            <p>
              <ul class="multicolumn texto-direita">
                ${ pokemon.estatisticas.map( lista2 => `<li>${ lista2 } </li>` ).join( '' ) }
            </ul>  
          </p>`

  divDadosPokemon.innerHTML = dados
  titleHtml.innerHTML = title
}

function idPokemonNaoEncontrado() {
  inputNroPokemon.value = ''
  const divDadosPokemon = document.querySelector( '.div-dadosPokemon' )
  const dados = `
          <p class="texto-alerta-esquerda">
            Pokémon não encontrado.<br>Digite um ID válido!
          </p>`
  divDadosPokemon.innerHTML = dados
  inputNroPokemon.focus()
}

function buscaPokemonAleatorio() {
  const nroAleatorio = Math.floor( Math.random() * 10 + 1 )

  if ( sessionStorage.getItem( nroAleatorio ) == null ) {
    sessionStorage.setItem( nroAleatorio, nroAleatorio )

    inputNroPokemon.value = nroAleatorio
    const pokemonEspecifico = pokeApi.buscarEspecifico( nroAleatorio )

    pokemonEspecifico
      .then( pokemon => {
        const poke = new Pokemon( pokemon )
        renderizacaoPokemon( poke )
      } )
      .catch( () => {
        idPokemonNaoEncontrado()
      } )
  } else {
    const divDadosPokemon = document.querySelector( '.div-dadosPokemon' )
    const dados = `
            <p class="texto-alerta-esquerda" style="left: 28%">
              Pokémon id ${ nroAleatorio }, já foi pesquisado!
            </p>`
    divDadosPokemon.innerHTML = dados
  }
}

function buscaPokemonEspecifico() {
  const pokemonEspecifico = pokeApi.buscarEspecifico( inputNroPokemon.value )

  if ( pokemonJaPesquisado === inputNroPokemon.value ) {
    customAlert( 'Este Pokémon já está sendo exibido!' )
  }

  pokemonEspecifico
    .then( pokemon => {
      const poke = new Pokemon( pokemon )
      renderizacaoPokemon( poke )
    } )
    .catch( () => {
      idPokemonNaoEncontrado()
    } )
  pokemonJaPesquisado = inputNroPokemon.value
}

/* ajustar pelo bloco de baixo.... método "buscar"
async function buscar() {
  const pokemonEspecifico = await pokeApi.buscarEspecifico( inputNroPokemon.value )
  const poke = new Pokemon( pokemonEspecifico );
  renderizacaoPokemon( poke )
}
buscar();
*/

inputNroPokemon.addEventListener( 'blur', () => {
  if ( inputNroPokemon.value !== '' && inputNroPokemon.value > 0 ) {
    buscaPokemonEspecifico()
  }
} )

buttonEtouComSorte.addEventListener( 'click', () => {
  buscaPokemonAleatorio()
} )
